<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Rent extends Model
{
    use LogsActivity;
    protected $table = 'rent';
    public $alias = 'rent';

    const DRAFT_STATUS = 0;
    const APPROVED_STATUS = 1;
    const CANCEL_STATUS = 2;
    const STATUS_ARR = [
        self::DRAFT_STATUS => 'wait_for_approval',
        self::APPROVED_STATUS => 'approved',
        self::CANCEL_STATUS => 'canceled'
    ];
    protected $fillable = ['asset_code', 'start_time','end_time','status','qty','unit','price','create_by','update_by'];

    public function university(): BelongsToMany
    {
        return $this->belongsToMany(University::class,'rent_universities','rent_id','university_id')
            ->withPivot('id','rent_id','university_id','created_at','updated_at');
    }

    public function company(): BelongsToMany
    {
        return $this->belongsToMany(Company::class,'rent_companies','rent_id','company_id')
            ->withPivot('id','rent_id','company_id','created_at','updated_at');
    }

    public function asset()
    {
        return $this->belongsTo(Asset::class, 'asset_code', 'code');
    }
    public function scopeWithAll($query)
    {
        $query->with('asset', 'university');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->useLogName('admin')
            ->logOnly(['*'])
            ->logOnlyDirty();
    }

    public function createBy(){
        return $this->belongsTo(User::class,'create_by','id');
    }
    public function updateBy(){
        return $this->belongsTo(User::class,'update_by','id');
    }
}
