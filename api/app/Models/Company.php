<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Company extends Model
{
    use LogsActivity;
    public $alias = 'company';
    protected $table = 'companies';
    protected $fillable = ['name','code','address','phone'];

    public function university(): BelongsToMany
    {
        return $this->belongsToMany(University::class,'companies_universities','company_id','university_id')
            ->withPivot('id','company_id','university_id','created_at','updated_at');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->useLogName('admin')
            ->logOnly(['*'])
            ->logOnlyDirty();
    }
}
