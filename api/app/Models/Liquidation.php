<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Liquidation extends Model
{
    use LogsActivity;
    protected $table = 'liquidations';
    public $alias = 'liquidation';

    const DRAFT_STATUS = 0;
    const APPROVED_STATUS = 1;
    const CANCEL_STATUS = 2;
    const STATUS_ARR = [
        self::DRAFT_STATUS => 'wait_for_approval',
        self::APPROVED_STATUS => 'approved',
        self::CANCEL_STATUS => 'canceled'
    ];
    protected $fillable = ['asset_code', 'start_time','end_time','status','qty','unit','price'];

    public function university(): BelongsToMany
    {
        return $this->belongsToMany(University::class,'liquidations_universities','liquidations_id','university_id')
            ->withPivot('id','liquidations_id','university_id','created_at','updated_at');
    }

    public function company(): BelongsToMany
    {
        return $this->belongsToMany(Company::class,'liquidations_companies','liquidations_id','company_id')
            ->withPivot('id','liquidations_id','company_id','created_at','updated_at');
    }

    public function asset()
    {
        return $this->belongsTo(Asset::class, 'asset_code', 'code');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->useLogName('admin')
            ->logOnly(['*'])
            ->logOnlyDirty();
    }
}
