<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class AssetItemAssetType extends Pivot
{
    use LogsActivity;
    protected $table = 'asset_item_asset_type';
    protected $fillable = ['asset_id','asset_type_id'];

    public function university(): BelongsTo
    {
        return $this->belongsTo(University::class,'university_id','id');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->useLogName('admin')
            ->logOnly(['*'])
            ->logOnlyDirty();
    }
}

