<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\FileAdder;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * @method void prepareToAttachMedia(Media $media, FileAdder $fileAdder)
 */
class Asset extends Model implements HasMedia
{
    use InteractsWithMedia, LogsActivity;

    const DRAFT_STATUS = 0;
    const WAITING_FOR_APPROVAL_STATUS = 1;
    const APPROVED_STATUS = 2;
    const LEASED_STATUS = 3;
    const LIQUIDATED_STATUS = 4;

    const STATUS_LIST = [
        self::DRAFT_STATUS => 'draft',
        self::WAITING_FOR_APPROVAL_STATUS => 'waiting_for_approval',
        self::APPROVED_STATUS => 'approved',
        self::LEASED_STATUS => 'leased',
        self::LIQUIDATED_STATUS => 'liquidated'
    ];

    public $alias = 'asset';

    protected $table = 'assets';
    protected $fillable = ['name', 'code', 'status', 'description'];

    public function assetType()
    {
        return $this->belongsToMany(AssetType::class, 'asset_item_asset_type', 'asset_id', 'asset_type_id')
            ->withPivot('id', 'asset_id', 'asset_type_id', 'created_at', 'updated_at');
    }

    public function university(): BelongsToMany
    {
        return $this->belongsToMany(University::class, 'assets_universities', 'asset_id', 'university_id')
            ->withPivot('id', 'asset_id', 'university_id', 'created_at', 'updated_at');
    }

    public function stock()
    {
        return $this->hasMany(AssetStock::class, 'asset_id');
    }

    public function rent()
    {
        return $this->hasMany(Rent::class, 'asset_code', 'code');
    }

    public function liquidations()
    {
        return $this->hasMany(Liquidation::class, 'asset_code', 'code');
    }

    public function scopeWithAll($query)
    {
        $query->with('assetType', 'university', 'rent', 'liquidations');
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('main_images');
        $this->addMediaConversion('thumb')
            ->crop('crop-center', 120, 120)
            ->performOnCollections('main_images');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->useLogName('admin')
            ->logOnly(['*'])
            ->logOnlyDirty();
    }
}
