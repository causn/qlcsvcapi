<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class AssetAttribute extends Model
{
    use LogsActivity;
    const TYPE_OPTIONS_CHOICES = ['text','select','select-value'];
    const TYPE_OPTIONS_TEXT = 'text';
    const TYPE_OPTIONS_SELECT = 'select';
    const TYPE_OPTIONS_SELECT_VALUE = 'select-value';

    public $alias = 'asset-attribute';

    protected $table = 'assets_attributes';

    protected $fillable = ['name','code','unit','type','value'];

    protected function assetType(): BelongsToMany
    {
        return $this->belongsToMany(AssetType::class,'assets_type_item_attribute','asset_attribute_id','asset_type_id');
    }

    public function university(): BelongsToMany
    {
        return $this->belongsToMany(University::class,'assets_attributes_universities','asset_attribute_id','university_id')
            ->withPivot('id','asset_attribute_id','university_id','created_at','updated_at');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->useLogName('admin')
            ->logOnly(['*'])
            ->logOnlyDirty();
    }
}
