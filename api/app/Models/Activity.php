<?php

namespace App\Models;

use Spatie\Activitylog\Models\Activity as ActivityVendor;
class Activity extends ActivityVendor
{
    protected $fillable = [
        'log_name', 'description', 'subject_type', 'event', 'subject_id', 'causer_type', 'causer_id' , 'batch_uuid','created_at','updated_at','token'
    ];
}
