<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class AssetType extends Model
{
    use LogsActivity;
    protected $table = 'assets_type';
    protected $fillable = ['name','code','description'];
    public $alias = 'asset-type';

    public function assetAttribute(){
        return $this->belongsToMany(AssetAttribute::class,'assets_type_item_attribute','asset_type_id','asset_attribute_id')
            ->withPivot('id','asset_type_id','asset_attribute_id','value','created_at','updated_at');
    }

    public function asset(){
        return $this->belongsToMany(Asset::class,'asset_item_asset_type','asset_type_id','asset_id')
            ->withPivot('id','asset_id','asset_type_id','created_at','updated_at');
    }

    public function university(): BelongsToMany
    {
        return $this->belongsToMany(University::class,'assets_type_universities','asset_type_id','university_id')
            ->withPivot('id','asset_type_id','university_id','created_at','updated_at');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->useLogName('admin')
            ->logOnly(['*'])
            ->logOnlyDirty();
    }
}
