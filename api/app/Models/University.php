<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class University extends Model implements HasMedia
{
    use LogsActivity, InteractsWithMedia;
    protected $table = 'universities';
    protected $fillable = ['name','code','address','phone'];
    public $alias = 'university';


    public function user(): BelongsToMany
    {
        return $this->belongsToMany(User::class,'users_universities','university_id','user_id')
            ->withPivot('id','user_id','university_id','created_at','updated_at');
    }
    public function asset(): BelongsToMany
    {
        return $this->belongsToMany(Asset::class,'assets_universities','university_id','asset_id')
            ->withPivot('id','asset_id','university_id','created_at','updated_at');
    }
    public function assetAttribute(): BelongsToMany
    {
        return $this->belongsToMany(AssetAttribute::class,'assets_attributes_universities','university_id','asset_attribute_id')
            ->withPivot('id','asset_attribute_id','university_id','created_at','updated_at');
    }
    public function assetType(): BelongsToMany
    {
        return $this->belongsToMany(AssetType::class,'assets_type_universities','university_id','asset_type_id')
            ->withPivot('id','asset_type_id','university_id','created_at','updated_at');
    }
    public function company(): BelongsToMany
    {
        return $this->belongsToMany(Company::class,'companies_universities','university_id','company_id')
            ->withPivot('id','company_id','university_id','created_at','updated_at');
    }
    public function liquidation(): BelongsToMany
    {
        return $this->belongsToMany(Liquidation::class,'liquidations_universities','university_id','liquidations_id')
            ->withPivot('id','liquidations_id','university_id','created_at','updated_at');
    }
    public function rent(): BelongsToMany
    {
        return $this->belongsToMany(Rent::class,'rent_universities','university_id','rent_id')
            ->withPivot('id','rent_id','university_id','created_at','updated_at');
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->useLogName('admin')
            ->logOnly(['*'])
            ->logOnlyDirty();
    }

    public function toArray()
    {
        $item = parent::toArray();
        $item['logo'] = $this->getMedia('logo')->toArray();
        $item['main_images'] = $this->getMedia('main_images')->toArray();
        return $item;
    }
}
