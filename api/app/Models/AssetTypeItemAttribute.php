<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class AssetTypeItemAttribute extends Pivot
{
    use LogsActivity;

    protected $table = 'assets_type_item_attribute';
    protected $fillable = ['asset_type_id', 'asset_attribute_id', 'value'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->useLogName('admin')
            ->logOnly(['*'])
            ->logOnlyDirty();
    }
}

