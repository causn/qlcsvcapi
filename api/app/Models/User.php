<?php

namespace App\Models;

use App\Notifications\VeriryRegisterEmail;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticate;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\FileAdder;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\Permission\Traits\HasRoles;

/**
 * @property mixed $id
 * @method void prepareToAttachMedia(Media $media, FileAdder $fileAdder)
 */
class User extends Authenticate implements MustVerifyEmail, HasMedia
{
    use HasFactory, Notifiable, HasRoles, HasApiTokens, InteractsWithMedia;
    public $alias = 'user';
    const DRAFT_STATUS = 0;
    const ACTIVE_STATUS = 1;
    protected $guard_name = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'email_verified_at', 'password', 'birthday', 'phone', 'address','is_admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return BelongsToMany
     */
    public function university(): BelongsToMany
    {
        return $this->belongsToMany(University::class,'users_universities','user_id','university_id')
            ->withPivot('id','user_id','university_id','created_at','updated_at');
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new VeriryRegisterEmail);
    }

    public function isAdmin(){
        return $this->is_admin == self::ACTIVE_STATUS;
    }
    public function hasVerifiedEmail()
    {
        return !is_null($this->email_verified_at);
    }

    public function markEmailAsVerified()
    {
        return $this->forceFill([
            'email_verified_at' => $this->freshTimestamp(),
        ])->save();
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('avatar');
        $this->addMediaConversion('thumb')
            ->crop('crop-center',120,120)
            ->performOnCollections('avatar');
    }
}
