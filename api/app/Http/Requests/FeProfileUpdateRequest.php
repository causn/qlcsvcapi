<?php

namespace App\Http\Requests;

use App\Rules\PhoneNumber;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class FeProfileUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => ['same:password_confirmation'],
            'phone' => [new PhoneNumber()],
            'birthday'=>[
                function ($attribute, $value, $fail) {
                    if(!empty($value)){
                        try{
                            Carbon::createFromFormat('d/m/Y', $value);
                        }catch (\Exception $exception){
                            $fail(Lang::get('validation.regex',['attribute'=>Lang::get('global.birthday')]));
                        }
                    }
                }
            ]
        ];
    }

    public function attributes()
    {
        return [
            'name'=>Lang::get('global.name'),
            'password'=>Lang::get('global.password'),
            'role'=>Lang::get('global.roles'),
            'university'=> Lang::get('global.university'),
            'phone' => Lang::get('global.Phone')
        ];
    }

    protected function passedValidation()
    {
        $inputs = $this->input();
        if(!empty($inputs['birthday'])){
            $birthday = Carbon::createFromFormat('d/m/Y', $inputs['birthday']);
            $this->merge(['birthday' => $birthday]);
        }
    }
}
