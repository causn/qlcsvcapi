<?php

namespace App\Http\Requests;

use App\Models\AssetType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class AssetTypeCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required'
        ];
    }

    protected function passedValidation()
    {
        $type = AssetType::orderBy('code', 'desc')->first();
        if ($type) {
            $max = sprintf("%03d", intval($type->code) + 1);
        } else {
            $max = sprintf("%03d", 1);
        }

        $this->merge([
            'code' => $max
        ]);
    }

    public function attributes()
    {
        return [
            'name' => Lang::get('global.name'),
            'description' => Lang::get('global.description')
        ];
    }
}
