<?php

namespace App\Http\Requests;

use App\Models\AssetAttribute;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class AssetAttributeCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'type' => 'required'
        ];
    }

    protected function passedValidation()
    {
        $attribute = AssetAttribute::orderBy('code', 'desc')->first();
        if($attribute){
            $max = sprintf("%03d", intval($attribute->code) + 1);
        }else{
            $max = sprintf("%03d", 1);
        }

        $this->merge([
            'code' => $max,
        ]);

        if($this->get('type') === AssetAttribute::TYPE_OPTIONS_SELECT){

            $keys = $this->get('select-key');
            $values = $this->get('select-value');
            $items = [];
            if(!empty($keys) && is_array($keys) && !empty($values) && is_array($values)) {
                for ($index = 0; $index < count($keys); $index++) {
                    if (!empty($keys[$index])) {
                        $items[$keys[$index]] = $values[$index];
                    }
                }
            }

        }elseif ($this->get('type') === AssetAttribute::TYPE_OPTIONS_SELECT_VALUE){

            $items = $this->get('select-value-value');
            $items = array_filter($items);

        }

        if(!empty($items)) {
            $this->merge([
                'value' => json_encode($items, JSON_UNESCAPED_UNICODE),
            ]);
        }
    }

    public function attributes()
    {
        return [
            'name' => Lang::get('global.name'),
            'unit' => Lang::get('global.unit'),
            'type' => Lang::get('global.type')
        ];
    }
}
