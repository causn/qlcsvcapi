<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LiquidationCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'asset_code' => ['required'],
            'start_time' => ['required'],
            'end_time' => ['required'],
            'status' => ['required'],
            'qty' => ['required','numeric','min:1'],
//            'price' => [
//                'required',
//                'regex:/^\d+(\.\d{1,2})?$/',
//                function ($attribute, $value, $fail) {
//                $value = floatval($value);
//                if($value <= 0){
//                    $fail(Lang::get('validation.gt.numeric',[
//                        'attribute' => Lang::get('global.price'),
//                        'value' => $value,
//                    ]));
//                }
//            }],
            'company' => ['required']
        ];
    }
}
