<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;
use App\Models\User;

class UserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required'],
            'email' => ['required','email','unique:users,email'],
            'password' => ['required','same:password_confirmation'],
            'role' => ['required'],
            'university' => function ($attribute, $value, $fail) {
                /** @var User $user */
                $user = auth()->user();

                if($user->hasRole(['administrator']) && empty($value)){
                    $fail(Lang::get('validation.required',['attribute'=>Lang::get('global.university')]));
                }
            },
        ];
    }

    protected function passedValidation()
    {
        /** @var User $user */
        $user = auth()->user();

        if(!$user->hasRole(['administrator']) && !empty($value)){
            $university = $user->university()->first();
            $this->merge([
                'university' => $university->id,
            ]);
        }

        $inputs = $this->input();

        if(!empty($inputs['is_admin']) && $inputs['is_admin'] == "on"){
            $this->merge(['is_admin' => User::ACTIVE_STATUS]);
        }else{
            $this->merge(['is_admin' => User::DRAFT_STATUS]);
        }
    }
}
