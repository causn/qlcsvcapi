<?php

namespace App\Http\Requests;

use App\Rules\PhoneNumber;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class RegisterRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'name' => ['required'],
            'email' => ['required'],
            'password' => ['required'],
            'password_confirmation' => ['same:password'],
            'birthday'=>[
                function ($attribute, $value, $fail) {
                    if(!empty($value)){
                        try{
                            Carbon::createFromFormat('d/m/Y', $value);
                        }catch (\Exception $exception){
                            $fail(Lang::get('validation.regex',['attribute'=>Lang::get('global.birthday')]));
                        }
                    }
                }
            ],
            'phone' => [new PhoneNumber()]
        ];
    }
    protected function passedValidation()
    {
        $inputs = $this->input();
        if(!empty($inputs['birthday'])){
            $birthday = Carbon::createFromFormat('d/m/Y', $inputs['birthday']);
            $this->merge(['birthday' => $birthday]);
        }
    }
}
