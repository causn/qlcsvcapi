<?php

namespace App\Http\Requests;

use App\Models\Company;
use Illuminate\Foundation\Http\FormRequest;

class CheckoutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'phone' => 'regex:/^\\+?\\d{1,4}?[-.\\s]?\\(?\\d{1,3}?\\)?[-.\\s]?\\d{1,4}[-.\\s]?\\d{1,4}[-.\\s]?\\d{1,9}$/'
        ];
    }

    public function messages()
    {
        return [

        ];
    }

    protected function passedValidation()
    {
        $company = Company::orderBy('code', 'desc')->first();
        if($company){
            $max = sprintf("%03d", (int)$company->code + 1);
        }else{
            $max = sprintf("%03d", 1);
        }

        $this->merge([
            'code' => $max,
        ]);
    }
}
