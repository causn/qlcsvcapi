<?php

namespace App\Http\Requests;

use App\Models\Asset;
use Illuminate\Foundation\Http\FormRequest;

class AssetCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'asset_type' => 'required'
        ];
    }

    protected function passedValidation()
    {
        $inputs = $this->input();

        if(!empty($inputs['status']) && $inputs['status'] == "on"){
            $this->merge(['status' => Asset::WAITING_FOR_APPROVAL_STATUS]);
        }else{
            $this->merge(['status' => Asset::DRAFT_STATUS]);
        }

        $company = Asset::orderBy('code', 'desc')->first();
        if($company){
            $max = sprintf("%03d", (int)$company->code + 1);
        }else{
            $max = sprintf("%03d", 1);
        }

        $this->merge(['code' => $max]);
    }
}
