<?php

namespace App\Http\Requests;

use App\Rules\PhoneNumber;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;
use App\Models\User;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => ['required'],
            'password' => ['same:password_confirmation'],
            'university' => function ($attribute, $value, $fail) {
                /** @var User $user */
                $user = auth()->user();

                if($user->hasRole(['administrator']) && empty($value) && $this->route('user')->id != $user->id){
                    $fail(Lang::get('validation.required',['attribute'=>Lang::get('global.university')]));
                }
            },
            'phone' => [new PhoneNumber()],
            'birthday'=>[
                function ($attribute, $value, $fail) {
                    if(!empty($value)){
                        try{
                            Carbon::createFromFormat('d/m/Y', $value);
                        }catch (\Exception $exception){
                            $fail(Lang::get('validation.regex',['attribute'=>Lang::get('global.birthday')]));
                        }
                    }
                }
            ]
        ];

        if($this->route('user')->id != auth()->user()->id){
            $rules['role'] = ['required'];
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'name'=>Lang::get('global.name'),
            'password'=>Lang::get('global.password'),
            'role'=>Lang::get('global.roles'),
            'university'=> Lang::get('global.university'),
            'phone' => Lang::get('global.Phone')
        ];
    }

    protected function passedValidation()
    {
        /** @var User $user */
        $user = auth()->user();

        if(!$user->hasRole(['administrator']) && !empty($value)){
            $university = $user->university()->first();
            $this->merge([
                'university' => $university->id,
            ]);
        }

        $inputs = $this->input();

        if(!empty($inputs['is_admin']) && $inputs['is_admin'] == "on"){
            $this->merge(['is_admin' => User::ACTIVE_STATUS]);
        }else{
            $this->merge(['is_admin' => User::DRAFT_STATUS]);
        }

        if(!empty($inputs['birthday'])){
            $birthday = Carbon::createFromFormat('d/m/Y', $inputs['birthday']);
            $this->merge(['birthday' => $birthday]);
        }
    }
}
