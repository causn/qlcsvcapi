<?php

namespace App\Http\Requests;

use App\Models\University;
use App\Rules\PhoneNumber;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;

class UniversityCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'phone' => [new PhoneNumber()],
            'birthday'=>[
                function ($attribute, $value, $fail) {
                    if(!empty($value)){
                        $object = Carbon::createFromFormat('d-m-Y', $value);
                        if (!$object instanceof Carbon) {
                            $fail(Lang::get('validation.regex',['attribute'=>Lang::get('global.birthday')]));
                        }
                    }
                }
            ]
        ];
    }

    protected function passedValidation()
    {
        $company = University::orderBy('code', 'desc')->first();
        if($company){
            $max = sprintf("%03d", (int)$company->code + 1);
        }else{
            $max = sprintf("%03d", 1);
        }

        $this->merge([
            'code' => $max,
        ]);
    }
}
