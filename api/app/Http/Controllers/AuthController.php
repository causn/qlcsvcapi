<?php

namespace App\Http\Controllers;

use App\Http\Traits\Helpers\ApiResponseTrait;
use App\Models\User;
use App\Repositories\Admin\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    use ApiResponseTrait;
    protected $userRepo;
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepo = $userRepository;
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        if ($validator->fails()) {
            return $this->respondValidationErrors($validator);
        }

        $user = $this->userRepo->register($request);
        if(!$user){
            return $this->respondInternalError();
        }

        $token = $user->createToken('auth_token')->plainTextToken;

        return $this->respondCreated([
            'access_token' => $token,
            'token_type'   => 'Bearer',
        ]);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'    => 'required|string',
            'password' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->respondValidationErrors($validator);
        }

        $credentials = $request->only(['email', 'password']);
        /** @var User $user */
        $user = User::query()->where('email',$credentials['email'])->first();

        if(!$user){
            return $this->respondError(trans('auth.failed'));
        }

        if(!$user->hasVerifiedEmail()){
            return $this->respondError(trans('auth.not_active'));
        }

        if(!Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            return $this->respondUnAuthorized();
        }

        Auth::login($user);

        $token = $user->createToken('auth_token',$user->getAllPermissions()->pluck('name','id')->toArray())->plainTextToken;

        return response()->json([
            'access_token' => $token,
            'token_type'   => 'Bearer',
        ],200);
    }
}
