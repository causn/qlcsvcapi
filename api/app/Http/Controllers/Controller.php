<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Laravel\Sanctum\PersonalAccessToken;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;
    protected $user;
    public function __construct(Request $request)
    {
        $personal = PersonalAccessToken::findToken($request->bearerToken());
        $user = User::find($personal->id);
        dd($personal->id,$personal,$user);
    }
}
