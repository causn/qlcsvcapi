<?php

namespace App\Http\Controllers

use App\Http\Controllers\Controller;
use App\Repositories\Admin\AssetRepository;
use App\Repositories\Admin\AssetTypeRepository;

class ApiAssetTypeController extends Controller
{
    protected $assetTypeRepo;
    public function __construct(AssetTypeRepository $assetTypeRepository)
    {
        $this->assetTypeRepo = $assetTypeRepository;
    }

    public function show($id){
        $assetType = $this->assetTypeRepo->find($id);
        $this->authorize('index',$assetType);
        dd("v");
        $html = view('admin.asset-type.detail', compact('assetType'))->render();

        return response()->json([
            'status' => true,
            'html' => $html,
            'message' => 'Getting messages successfully.',
        ]);
    }
}
