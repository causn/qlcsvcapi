<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Traits\Helpers\ApiResponseTrait;
use App\Repositories\Admin\AccessSystemRepository;
use App\Repositories\Admin\UserRepository;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Request;

class AccessSystemController extends Controller
{
    use ApiResponseTrait;
    protected $accessSystemRepo;
    protected $userRepo;
    public function __construct(
        AccessSystemRepository $accessSystemRepository,
        UserRepository $userRepository
    )
    {
        $this->accessSystemRepo = $accessSystemRepository;
        $this->userRepo = $userRepository;
    }

    public function index(Request $request){
        $userRequest = new Request();
        $users = $this->userRepo->getListWithFilter($userRequest)->pluck('id')->toArray();
        $request->merge(['users' => $users]);

        if(request()->ajax()) {
            return datatables()->of($this->accessSystemRepo->getListWithFilter($request))
                ->addColumn('name',function($model) {
                    $user = $model->causer()->first();
                    if($user){
                        return $user->name;
                    }
                    return  '';
                })
                ->addColumn('email',function($model) {
                    $user = $model->causer()->first();
                    if($user){
                        return $user->email;
                    }
                    return  '';
                })
                ->addColumn('ip',function ($model){
                    $locationArr = $model->getExtraProperty('location');
                    return $locationArr['ip'] ?? '';
                })
                ->editColumn('location', function($model) {
                    $locationArr = $model->getExtraProperty('location');
                    $str = '';
                    if(!empty($locationArr['city'])) {
                        $str .= Lang::get('global.city').' :' . $locationArr['city'];
                    }

                    if(!empty($locationArr['country'])) {
                        $str .= '<br>'.Lang::get('global.country') . ': ' . $locationArr['country'];

                    }
                    return $str;
                })
                ->addColumn('login_at',function ($model){
                    $logTime = explode(",",$model->log_time);
                    return $logTime[0] ?? '';
                })
                ->addColumn('logout_at',function($model){
                    $logTime = explode(",",$model->log_time);
                    return $logTime[1] ?? '';
                })
                ->make(true);
        }

        return view('admin.access-system.index');
    }
}
