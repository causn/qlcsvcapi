<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Traits\Helpers\ApiResponseTrait;
use App\Repositories\Admin\SystemDatabaseRepository;
use App\Repositories\Admin\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class SystemDatabaseController extends Controller
{
    use ApiResponseTrait;
    protected $uersRepo;
    protected $systemDatabaseRepo;
    public function __construct(UserRepository  $userRepository,SystemDatabaseRepository $systemDatabaseRepository)
    {
        $this->uersRepo = $userRepository;
        $this->systemDatabaseRepo = $systemDatabaseRepository;
    }

    public function index(Request  $request){
        if(request()->ajax()) {
            $data = $this->systemDatabaseRepo->getListWithFilter($request);
            return datatables()->of($data)
                ->addColumn('action',function ($item){
                    return view('admin.system-database.action',['item'=>$item]);
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('admin.system-database.index');
    }

    public function backup(Request $request)
    {
        $status = $this->systemDatabaseRepo->backup($request);
        if($status['status']) {
            return redirect()->route('admin.access_system_database')
                ->with('success', Lang::get('global.backup success'));
        }

        return redirect()->back()->with('danger',Lang::get('global.backup fail'));
    }

    public function delete(Request $request,$name,$fileName)
    {
        $status = $this->systemDatabaseRepo->delete($request,$name,$fileName);
        if($status['status']) {
            return redirect()->route('admin.access_system_database')
                ->with('success', Lang::get('global.delete success'));
        }

        return redirect()->back()->with('danger',Lang::get('global.delete fail'));
    }

    public function restore(Request $request,$name,$fileName)
    {
        try {
            $status = $this->systemDatabaseRepo->backup($request);
            if($status['status']) {
                $this->systemDatabaseRepo->restore($request, $name, $fileName);
                return redirect()->route('admin.access_system_database')
                    ->with('success', Lang::get('global.restore success'));
            }else{
                return redirect()->back()->with('danger', Lang::get('global.Database restore failed because the backup at this time contained an error'));
            }
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('danger', Lang::get('global.restore fail'));
        }
    }
}
