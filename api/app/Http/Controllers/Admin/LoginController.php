<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Traits\Helpers\ApiResponseTrait;
use App\Models\User;
use App\Repositories\Admin\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    use ApiResponseTrait;
    protected $userRepo;
    public function __construct(UserRepository  $userRepository)
    {
        $this->userRepo = $userRepository;
    }

    public function login(Request $request)
    {
        if ($request->getMethod() == 'GET') {
            return view('admin.auth.login');
        }

        $credentials = $request->only(['email', 'password']);

        $user = User::query()->where('email',$credentials['email'])->first();

        if($user) {
            $credentialsAdmin = $credentials;
            $credentialsWeb = $credentials;

            $credentialsAdmin['is_admin'] = User::ACTIVE_STATUS;
            $credentialsWeb['is_admin'] = User::DRAFT_STATUS;
            $location = geoip(getIpPublic())->toArray();

            $token = $user->id . "|" . Str::random(10) . "|" . $user->password;
            $request->session()->put('access_token',$token);

            if (Auth::guard('admin')->attempt($credentialsAdmin) && $user->hasVerifiedEmail()) {
                $activity = activity()->causedBy($user)
                    ->useLog('admin')
                    ->withProperties(['location' => $location])
                    ->performedOn($user)
                    ->event('logged')
                    ->log('logged');

                $activity->batch_uuid = $token;
                $activity->token = $token;
                $activity->save();

                return redirect()->route('admin.home');
            }

            if (Auth::guard('web')->attempt($credentialsWeb) && $user->hasVerifiedEmail()) {
                $activity = activity()->causedBy($user)
                    ->useLog('web')
                    ->withProperties(['location' => $location])
                    ->performedOn($user)
                    ->event('logged')
                    ->log('logged');

                $activity->batch_uuid = $token;
                $activity->token = $token;
                $activity->save();

                return redirect()->route('profile');
            }

            if ($user->is_admin == User::DRAFT_STATUS) {
                Auth::guard('web')->attempt($credentialsWeb);
                $activity = activity()->causedBy($user)
                    ->useLog('web')
                    ->withProperties(['location' => $location])
                    ->performedOn($user)
                    ->event('logged')
                    ->log('logged');

                $activity->batch_uuid = $token;
                $activity->token = $token;
                $activity->save();

                return redirect()->route('profile');
            }
        }

        return redirect()->back()->withInput()->withErrors(['email'=>Lang::get('global.email_or_password_not_correct')]);
    }

    public function logout(Request $request){
        $location = geoip(getIpPublic())->toArray();
        $accessToken = $request->session()->get('access_token');

        $activity = activity()->causedBy(auth()->user())
            ->useLog('admin')
            ->event('logged-out')
            ->performedOn(auth()->user())
            ->withProperties(['location' => $location])
            ->event('logged-out')
            ->log('logged-out');

        $activity->batch_uuid = $accessToken;
        $activity->token = $accessToken;
        $activity->save();

        Session::flush();
        Auth::logout();

        return redirect(route('admin.login'));
    }
}
