<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UniversityCreateRequest;
use App\Http\Requests\UniversityUpdateRequest;
use App\Http\Traits\Helpers\ApiResponseTrait;
use App\Models\University;
use App\Repositories\Admin\UniversityRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class UniversityController extends Controller
{
    use ApiResponseTrait;
    protected $universityRepo;

    public function __construct(UniversityRepository $universityRepository){
        $this->authorizeResource(University::class,'university');
        $this->universityRepo = $universityRepository;
    }

    public function index(Request $request)
    {
        if(request()->ajax()) {
            $data = $this->universityRepo->getListWithFilter($request);
            return datatables()->of($data)
                ->addColumn('action',function ($university){
                    return view('admin.university.action',['university'=>$university]);
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('admin.university.index');
    }

    public function create()
    {
        return view('admin.university.create');
    }

    public function store(UniversityCreateRequest $request)
    {
        if($this->universityRepo->create($request)) {
            return redirect()->route('admin.university.index')
                ->with('success', Lang::get('global.university_created_successfully'));
        }

        return redirect()->back()->with('danger',Lang::get('global.create_fail'));
    }

    public function edit(University $university)
    {
        $mainImages = $university->getMedia('main_images');
        $logo = $university->getMedia('logo');

        return view('admin.university.edit',compact('university','mainImages','logo'));
    }

    public function update(UniversityUpdateRequest $request,University $university)
    {
        if ($this->universityRepo->update($university, $request)) {
            return redirect()->route('admin.university.index')
            ->with('success',Lang::get('global.university_updated_successfully'));
        }

        return redirect()->back()->with('danger',Lang::get('global.update_fail'));
    }

    public function destroy(University $university)
    {
        if($this->universityRepo->destroy($university)){
            return redirect()->route('admin.university.index')
            ->with('success',Lang::get('global.university_deleted_successfully'));
        }

        return redirect()->route('admin.university.index')
            ->with('danger',Lang::get('global.delete_fail'));
    }
}
