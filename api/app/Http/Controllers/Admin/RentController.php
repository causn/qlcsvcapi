<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RentCreateRequest;
use App\Http\Requests\RentUpdateRequest;
use App\Http\Traits\Helpers\ApiResponseTrait;
use App\Models\Asset;
use App\Models\Rent;
use App\Repositories\Admin\AssetRepository;
use App\Repositories\Admin\CompanyRepository;
use App\Repositories\Admin\RentRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class RentController extends Controller
{
    use ApiResponseTrait;
    protected $rentRepo;
    protected $assetRepo;
    protected $companyRepo;
    public function __construct(
        RentRepository $rentRepository,
        AssetRepository $assetRepository,
        CompanyRepository  $companyRepository
    )
    {
        $this->authorizeResource(Rent::class,'rent');
        $this->rentRepo = $rentRepository;
        $this->assetRepo = $assetRepository;
        $this->companyRepo = $companyRepository;
    }

    public function index(Request $request)
    {
        if(request()->ajax()) {
            return datatables()->of($this->rentRepo->getListWithFilter($request))
                ->addColumn('asset_title',function($rent){
                  return $rent->asset->name;
                })
                ->addColumn('action',function ($rent){
                    return view('admin.rent.action',['rent'=>$rent]);
                })
                ->addColumn('status_title',function ($rent){
                    return Lang::get('global.'.Rent::STATUS_ARR[$rent->status]);
                })
                ->addColumn('time',function ($rent){
                    $time = [];

                    if($rent->start_time != '0000-00-00 00:00:00' && !empty($rent->start_time)){
                        $start = Carbon::createFromFormat('Y-m-d H:i:s', $rent->start_time);
                        $time[] = $start->format('d/m/Y');
                    }

                    if($rent->end_time != '0000-00-00 00:00:00' && !empty($rent->end_time)){
                        $end = Carbon::createFromFormat('Y-m-d H:i:s', $rent->end_time);
                        $time[] = $end->format('d/m/Y');
                    }

                    if(count($time) > 0){
                        return implode(" - ",$time);
                    }else{
                        return '';
                    }
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }

        return view('admin.rent.index');
    }

    public function create(Request $request)
    {
        if(!$request->has('status')){
            $request->merge(['status'=>Asset::APPROVED_STATUS]);
        }
        $assets = $this->assetRepo->getListWithFilter($request);
        $companies = $this->companyRepo->getListWithFilter($request);
        return view('admin.rent.create',compact('assets','companies'));
    }

    public function store(RentCreateRequest $request)
    {
        $inputs = $request->all();

        $newRequest = new Request();
        $newRequest->merge([
            'code' => $inputs['asset_code'],
            'start_time' => $inputs['start_time'],
            'end_time' => $inputs['end_time']
        ]);

        $asset = $this->assetRepo->getAssetAreInUse($newRequest);

        if(!$asset){
            return redirect()->back()->with('danger',
                Lang::get('global.asset_does_not_exist',[
                    'start' => $inputs['start_time'],
                    'end' =>$inputs['end_time'],
                ])
            )->withInput($inputs);
        }

        if($this->rentRepo->create($request)) {
            return redirect()->route('admin.rent.index')
                ->with('success', Lang::get('global.rent_created_successfully'));
        }

        return redirect()->back()->with('danger',Lang::get('global.create_fail'))->withInput($inputs);
    }
    public function show(Rent $rent)
    {
        return view('admin.rent.show',compact('rent'));
    }

    public function edit(Request $request,Rent $rent)
    {
        $rent = $this->rentRepo->formatOutput($rent);
        $assets = $this->assetRepo->getListWithFilter($request);
        $companies = $this->companyRepo->getListWithFilter($request);
        return view('admin.rent.edit',compact('rent','assets','companies'));
    }

    public function update(RentUpdateRequest $request, Rent $rent)
    {
        $inputs = $request->all();

        $newRequest = new Request();
        $newRequest->merge([
            'code' => $inputs['asset_code'],
            'start_time' => $inputs['start_time'],
            'end_time' => $inputs['end_time']
        ]);

        $asset = $this->assetRepo->getAssetAreInUse($newRequest);

        if(!$asset){
            return redirect()->back()->with('danger',
                Lang::get('global.asset_does_not_exist',[
                    'start' => $inputs['start_time'],
                    'end' =>$inputs['end_time'],
                ])
            )->withInput($inputs);
        }

        if ($this->rentRepo->update($rent, $request)) {
            return redirect()->route('admin.rent.index')
                ->with('success',Lang::get('global.rent_updated_successfully'));
        }

        return redirect()->back()->with('danger',Lang::get('global.update_fail'))->withInput($inputs);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Rent $rent)
    {
        if($this->rentRepo->destroy($rent)){
            return redirect()->route('admin.rent.index')
                ->with('success',Lang::get('global.rent_deleted_successfully'));
        }

        return redirect()->route('admin.permissions.index')
            ->with('danger',Lang::get('global.delete_fail'));
    }
}
