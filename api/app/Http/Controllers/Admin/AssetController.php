<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AssetCreateRequest;
use App\Http\Requests\AssetUpdateRequest;
use App\Http\Traits\Helpers\ApiResponseTrait;
use App\Models\Asset;
use App\Repositories\Admin\AssetAttributeRepository;
use App\Repositories\Admin\AssetRepository;
use App\Repositories\Admin\AssetTypeRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Lang;

class AssetController extends Controller
{
    use ApiResponseTrait;
    protected $assetRepo;
    protected $assetTypeRepo;
    protected $assetAttributeRepo;

    public function __construct(
        AssetRepository          $assetRepository,
        AssetTypeRepository      $assetTypeRepository,
        AssetAttributeRepository $assetAttributeRepository
    )
    {
        $this->authorizeResource(Asset::class, 'asset');
        $this->assetRepo = $assetRepository;
        $this->assetTypeRepo = $assetTypeRepository;
        $this->assetAttributeRepo = $assetAttributeRepository;
    }

    public function index(Request $request)
    {
        $jsonResource = new JsonResource([
            'data' => $this->assetRepo->cursorPaginate($request)
        ]);
        return $this->respondWithResource($jsonResource);
    }

    public function store(AssetCreateRequest $request)
    {
        $asset = $this->assetRepo->create($request);
        if ($asset) {
            $jsonResource = new JsonResource([
                'data' => $asset
            ]);
            return $this->respondWithResource($jsonResource,Lang::get('global.company_created_successfully'));
        }

        return $this->respondError(Lang::get('global.create_fail'));
    }

    public function detail(Request $request, Asset $asset)
    {
        $assetTypes = $this->assetTypeRepo->getListWithFilter($request);
        $mainImages = $asset->getMedia('main_images');
        foreach ($mainImages as &$image) {
            $image->thumb_url = $image->getUrl('thumb');
        }

        $jsonResource = new JsonResource([
            'data' => [
                'asset' => $asset,
                'assetTypes' => $assetTypes,
                'mainImages' => $mainImages,
            ]
        ]);
        return $this->respondWithResource($jsonResource);
    }

    public function update(AssetUpdateRequest $request, Asset $asset)
    {
        $newAsset = $this->assetRepo->update($asset,$request);
        if (!$newAsset) {
            return $this->respondError(Lang::get('global.update_fail'));
        }

        $jsonResource = new JsonResource([
            'data' => $newAsset
        ]);
        return $this->respondWithResource($jsonResource,Lang::get('global.asset_updated_successfully'));
    }

    public function destroy(Asset $asset)
    {
        if ($asset->delete()) {
            return redirect()->route('admin.asset.index')
                ->with('success', Lang::get('global.asset_deleted_successfully'));
        }

        return redirect()->route('admin.asset.index')
            ->with('error', Lang::get('global.delete_fail'));

    }

    public function filter(Request $request)
    {
        $asset = $this->assetRepo->getAssetAreInUse($request);

        if ($asset) {
            $jsonResource = new JsonResource([
                'data' => $asset
            ]);
            return $this->respondWithResource($jsonResource,'Getting messages successfully.');
        }

        return $this->respondError(Lang::get('global.asset_does_not_exist'));
    }

    protected function resourceAbilityMap()
    {
        return [
            'index' => 'index',
            'create' => 'store',
            'store' => 'store',
            'edit' => 'update',
            'update' => 'update',
            'destroy' => 'destroy',
            'filter' => 'index'
        ];
    }

    protected function resourceMethodsWithoutModels()
    {
        return ['index', 'create', 'store', 'filter'];
    }
}
