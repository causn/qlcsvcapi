<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RoleCreateRequest;
use App\Http\Requests\RoleUpdateRequest;
use App\Http\Traits\Helpers\ApiResponseTrait;
use App\Models\Role;
use App\Repositories\Admin\PermissionRepository;
use App\Repositories\Admin\RoleRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class RoleController extends Controller
{
    use ApiResponseTrait;
    protected $roleRepo;
    protected $permissionRepo;

    public function __construct(
        RoleRepository $roleRepository,
        PermissionRepository $permissionRepository
    )
    {
        $this->authorizeResource(Role::class,'role');
        $this->roleRepo = $roleRepository;
        $this->permissionRepo = $permissionRepository;
    }

    public function index(Request $request)
    {
        if(request()->ajax()) {
            return datatables()->of($this->roleRepo->getListWithFilter($request))
                ->addColumn('action',function ($role){
                    return view('admin.roles.action',['role'=>$role]);
                })
                ->addColumn('permissions',function ($role) {
                    $result = [];
                    $permissions =  $role->permissions()->pluck('name')->toArray() ?? [];
                    foreach($permissions as $permission){
                        $result[] = Lang::get('global.'.$permission);
                    }
                    return $result;

                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }

        return view('admin.roles.index');
    }

    public function create(Request $request)
    {
        $permissions = $this->permissionRepo->getListWithFilter($request);
        return view('admin.roles.create',compact('permissions'));
    }

    public function store(RoleCreateRequest $request)
    {
        if($this->permissionRepo->create($request)) {
            return redirect()->route('admin.roles.index')
                ->with('success', Lang::get('global.role_created_successfully'));
        }

        return redirect()->back()->with('danger',Lang::get('global.create_fail'));
    }


    public function edit(Request $request,Role $role)
    {
        $permissions = $this->permissionRepo->getListWithFilter($request);

        return view('admin.roles.edit',compact('role','permissions'));
    }

    public function update(RoleUpdateRequest $request,Role $role)
    {
        if ($this->roleRepo->update($role, $request)) {
            return redirect()->route('admin.roles.index')
            ->with('success',Lang::get('global.role_updated_successfully'));
        }

        return redirect()->back()->with('danger',Lang::get('global.update_fail'));
    }

    public function destroy(Role $role)
    {
        if($this->roleRepo->destroy($role)){
            return redirect()->route('admin.roles.index')
            ->with('success',Lang::get('global.role_deleted_successfully'));
        }

        return redirect()->route('admin.roles.index')
            ->with('danger',Lang::get('global.delete_fail'));
    }
}
