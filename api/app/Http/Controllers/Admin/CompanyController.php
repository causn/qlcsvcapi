<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyCreateRequest;
use App\Http\Requests\CompanyUpdateRequest;
use App\Http\Traits\Helpers\ApiResponseTrait;
use App\Models\Company;
use App\Repositories\Admin\CompanyRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Lang;

class CompanyController extends Controller
{
    use ApiResponseTrait;
    protected $companyRepo;
    public function __construct(CompanyRepository $companyRepository)
    {
        $this->authorizeResource(Company::class, 'company');
        $this->companyRepo = $companyRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $jsonResource = new JsonResource([
            'data' => $this->companyRepo->cursorPaginate($request)
        ]);
        return $this->respondWithResource($jsonResource);
    }

    /**
     * @param CompanyCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(CompanyCreateRequest $request)
    {
        $company = $this->companyRepo->create($request);
        if(!$company){
            return $this->respondError(Lang::get('global.create_fail'));
        }

        $jsonResource = new JsonResource([
            'data' => $company
        ]);
        return $this->respondWithResource($jsonResource,Lang::get('global.company_created_successfully'));
    }

    public function detail(Company $company)
    {
        $jsonResource = new JsonResource([
            'data' => $company
        ]);
        return $this->respondWithResource($jsonResource);
    }

    public function update(CompanyUpdateRequest $request, Company $company)
    {
        $newCompany = $this->companyRepo->update($company,$request);
        if (!$newCompany) {
            return $this->respondError(Lang::get('global.update_fail'));
        }

        $jsonResource = new JsonResource([
            'data' => $newCompany
        ]);
        return $this->respondWithResource($jsonResource,Lang::get('global.company_updated_successfully'));
    }


    public function destroy(Company $company)
    {
        if($this->companyRepo->destroy($company)){
            return $this->respondSuccess(Lang::get('global.company_deleted_successfully'));
        }

        return $this->respondError(Lang::get('global.delete_fail'));
    }
}
