<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AssetTypeCreateRequest;
use App\Http\Requests\AssetTypeUpdateRequest;
use App\Http\Traits\Helpers\ApiResponseTrait;
use App\Models\AssetType;
use App\Repositories\Admin\AssetAttributeRepository;
use App\Repositories\Admin\AssetTypeRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class AssetTypeController extends Controller
{
    use ApiResponseTrait;
    protected $assetTypeRepo;
    protected $assetAttributeRepo;

    public function __construct(
        AssetTypeRepository $assetTypeRepository,
        AssetAttributeRepository $assetAttributeRepository
    ){
        $this->authorizeResource(AssetType::class,'assetType');
        $this->assetTypeRepo = $assetTypeRepository;
        $this->assetAttributeRepo = $assetAttributeRepository;
    }

    public function index(Request $request)
    {
        if(request()->ajax()) {
            return datatables()->of($this->assetTypeRepo->getListWithFilter($request))
                ->addColumn('action',function ($assetType){
                    return view('admin.asset-type.action',['assetType'=>$assetType]);
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('admin.asset-type.index');
    }

    public function create(Request $request)
    {
        $assetAttributes = $this->assetAttributeRepo->getListWithFilter($request);

        return view('admin.asset-type.create',compact('assetAttributes'));
    }

    public function store(AssetTypeCreateRequest $request)
    {
        if($this->assetTypeRepo->create($request)) {
            return redirect()->route('admin.asset-type.index')
                ->with('success', Lang::get('global.asset_type_created_successfully'));
        }

        return redirect()->back()->with('danger',Lang::get('global.create_fail'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $assetType = $this->assetTypeRepo->find($id);

        $html = view('admin.asset-type.detail', compact('assetType'))->render();

        return response()->json([
            'status' => true,
            'html' => $html,
            'message' => 'Getting messages successfully.',
        ]);
    }

    public function edit(Request $request,AssetType $assetType)
    {
        $assetAttributes = $this->assetAttributeRepo->getListWithFilter($request);

        return view('admin.asset-type.edit',compact('assetType','assetAttributes'));
    }

    public function update(AssetTypeUpdateRequest $request, AssetType $assetType)
    {
        if ($this->assetTypeRepo->update($assetType, $request)) {
            return redirect()->route('admin.asset-type.index')
            ->with('success',Lang::get('global.asset_type_updated_successfully'));
        }

        return redirect()->back()->with('danger',Lang::get('global.update_fail'));
    }

    public function destroy(AssetType $assetType)
    {
        if($this->assetTypeRepo->destroy($assetType)){
            return redirect()->route('admin.asset-type.index')
            ->with('success',Lang::get('global.asset_type_deleted_successfully'));
        }

        return redirect()->route('admin.asset-type.index')
            ->with('danger',Lang::get('global.delete_fail'));
    }

    public function filter($id)
    {
        $assetType = $this->assetTypeRepo->find($id);

        $html = view('admin.asset-type.detail', compact('assetType'))->render();

        return response()->json([
            'status' => true,
            'html' => $html,
            'message' => 'Getting messages successfully.',
        ]);
    }

    protected function resourceAbilityMap()
    {
        return [
            'index' => 'index',
            'create' => 'store',
            'store' => 'store',
            'edit' => 'update',
            'update' => 'update',
            'destroy' => 'destroy',
        ];
    }

    protected function resourceMethodsWithoutModels()
    {
        return ['index', 'create', 'store','edit'];
    }
}
