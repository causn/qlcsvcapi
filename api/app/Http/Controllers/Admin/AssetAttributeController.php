<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AssetAttributeCreateRequest;
use App\Http\Requests\AssetAttributeUpdateRequest;
use App\Http\Traits\Helpers\ApiResponseTrait;
use App\Models\AssetAttribute;
use App\Repositories\Admin\AssetAttributeRepository;
use App\Repositories\Admin\UniversityRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class AssetAttributeController extends Controller
{
    use ApiResponseTrait;
    protected $assetAttributeRepo;
    protected $universityRepo;

    public function __construct(
        AssetAttributeRepository $assetAttributeRepository,
        UniversityRepository  $universityRepository
    ){
        $this->authorizeResource(AssetAttribute::class,'assetAttribute');
        $this->assetAttributeRepo = $assetAttributeRepository;
        $this->universityRepo = $universityRepository;
    }

    public function index(Request $request)
    {
        if(request()->ajax()) {
            return datatables()->of($this->assetAttributeRepo->getListWithFilter($request))
                ->addColumn('action',function ($assetAttribute){
                    return view('admin.asset-attribute.action',['assetAttribute'=>$assetAttribute]);
                })
                ->editColumn('code',function ($assetAttribute){
                    return 'AT'.$assetAttribute->code;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('admin.asset-attribute.index');
    }

    public function create()
    {
        return view('admin.asset-attribute.create');
    }

    public function store(AssetAttributeCreateRequest $request)
    {
        if($this->assetAttributeRepo->create($request)){
            return redirect()->route('admin.asset-attribute.index')
            ->with('success',Lang::get('global.asset_attribute_created_successfully'));
        }

        return redirect()->back()->with('danger',Lang::get('global.create_fail'));
    }

    public function edit(Request $request,AssetAttribute $assetAttribute)
    {
        if(auth()->user()->hasRole(['administrator'])) {
            $universities = $this->universityRepo->getListWithFilter($request);
            return view('admin.asset-attribute.edit',compact('assetAttribute','universities'));
        }

        return view('admin.asset-attribute.edit',compact('assetAttribute'));
    }


    public function update(AssetAttributeUpdateRequest $request,AssetAttribute $assetAttribute)
    {
        if ($this->assetAttributeRepo->update($assetAttribute, $request)) {
            return redirect()->route('admin.asset-attribute.index')
            ->with('success',Lang::get('global.asset_attribute_updated_successfully'));
        }

        return redirect()->back()->with('danger',Lang::get('global.update_fail'));
    }


    public function destroy(AssetAttribute $assetAttribute)
    {
        if($this->assetAttributeRepo->destroy($assetAttribute)){
            return redirect()->route('admin.asset-attribute.index')
            ->with('success',Lang::get('global.asset_attribute_deleted_successfully'));
        }

        return redirect()->route('admin.users.index')
            ->with('danger',Lang::get('global.delete_fail'));
    }
}
