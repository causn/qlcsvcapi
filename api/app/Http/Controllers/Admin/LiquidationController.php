<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\LiquidationCreateRequest;
use App\Http\Requests\LiquidationUpdateRequest;
use App\Http\Traits\Helpers\ApiResponseTrait;
use App\Models\Asset;
use App\Models\Liquidation;
use App\Repositories\Admin\AssetRepository;
use App\Repositories\Admin\CompanyRepository;
use App\Repositories\Admin\LiquidationRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Carbon\Carbon;

class LiquidationController extends Controller
{
    use ApiResponseTrait;
    protected $liquidationRepo;
    protected $assetRepo;
    protected $companyRepo;
    public function __construct(
        LiquidationRepository $liquidationRepository,
        AssetRepository  $assetRepository,
        CompanyRepository  $companyRepository
    )
    {
        $this->authorizeResource(Liquidation::class,'liquidation');
        $this->liquidationRepo = $liquidationRepository;
        $this->assetRepo = $assetRepository;
        $this->companyRepo = $companyRepository;
    }

    public function index(Request $request)
    {
        if(request()->ajax()) {
            return datatables()->of($this->liquidationRepo->getListWithFilter($request))
                ->addColumn('asset_title',function($liquidation){
                  return $liquidation->asset->name;
                })
                ->addColumn('action',function ($liquidation){
                    return view('admin.liquidation.action',['liquidation'=>$liquidation]);
                })
                ->addColumn('status_title',function ($liquidation){
                    return Lang::get('global.'.Liquidation::STATUS_ARR[$liquidation->status]);
                })
                ->addColumn('time',function ($liquidation){
                    $time = [];

                    if($liquidation->start_time != '0000-00-00 00:00:00' && !empty($liquidation->start_time)){
                        $start = Carbon::createFromFormat('Y-m-d H:i:s', $liquidation->start_time);
                        $time[] = $start->format('d/m/Y');
                    }

                    if($liquidation->end_time != '0000-00-00 00:00:00' && !empty($liquidation->end_time)){
                        $end = Carbon::createFromFormat('Y-m-d H:i:s', $liquidation->end_time);
                        $time[] = $end->format('d/m/Y');
                    }

                    if(count($time) > 0){
                        return implode(" - ",$time);
                    }else{
                        return '';
                    }
                })
                ->editColumn('price',function ($liquidation){
                    return number_format($liquidation->price).' '.$liquidation->unit;
                })
                ->addColumn('total_price',function ($liquidation){
                    $qty = $liquidation->qty ?? 0;
                    return number_format($qty*$liquidation->price).' '.$liquidation->unit;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }

        return view('admin.liquidation.index');
    }

    public function create(Request $request)
    {
        if(!$request->has('status')){
            $request->merge(['status'=>Asset::APPROVED_STATUS]);
        }
        $assets = $this->assetRepo->getActive($request,[],null,'liquidation');
        $companies = $this->companyRepo->getListWithFilter($request);
        return view('admin.liquidation.create',compact('assets','companies'));
    }

    public function store(LiquidationCreateRequest $request)
    {
        $inputs = $request->all();

        $newRequest = new Request();
        $newRequest->merge([
            'code' => $inputs['asset_code'],
            'start_time' => $inputs['start_time'],
            'end_time' => $inputs['end_time']
        ]);

        $asset = $this->assetRepo->getAssetAreInUse($newRequest);

        if(!$asset){
            return redirect()->back()->with('danger',
                Lang::get('global.asset_does_not_exist',[
                    'start' => $inputs['start_time'],
                    'end' =>$inputs['end_time'],
                ])
            )->withInput($inputs);
        }

        if($this->liquidationRepo->create($request)) {
            return redirect()->route('admin.liquidation.index')
                ->with('success', Lang::get('global.liquidation_created_successfully'));
        }

        return redirect()->back()->with('danger',Lang::get('global.create_fail'))->withInput($inputs);
    }

    public function show(Liquidation $liquidation)
    {
        return view('admin.liquidation.show',compact('liquidation'));
    }

    public function edit(Request $request,Liquidation $liquidation)
    {
        $assets = $this->assetRepo->getListWithFilter($request);
        $companies = $this->companyRepo->getListWithFilter($request);
        return view('admin.liquidation.edit',compact('liquidation','assets','companies'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(LiquidationUpdateRequest $request,Liquidation $liquidation)
    {
        $inputs = $request->all();

        $newRequest = new Request();
        $newRequest->merge([
            'code' => $inputs['asset_code'],
            'start_time' => $inputs['start_time'],
            'end_time' => $inputs['end_time']
        ]);

        $asset = $this->assetRepo->getAssetAreInUse($newRequest);

        if(!$asset){
            return redirect()->back()->with('danger',
                Lang::get('global.asset_does_not_exist',[
                    'start' => $inputs['start_time'],
                    'end' =>$inputs['end_time'],
                ])
            )->withInput($inputs);
        }

        if ($this->liquidationRepo->update($liquidation, $request)) {
            return redirect()->route('admin.rent.index')
                ->with('success',Lang::get('global.liquidation_updated_successfully'));
        }

        return redirect()->back()->with('danger',Lang::get('global.update_fail'))->withInput($inputs);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Liquidation $liquidation)
    {
        if($this->liquidationRepo->destroy($liquidation)){
            return redirect()->route('admin.liquidation.index')
                ->with('success',Lang::get('global.liquidation_deleted_successfully'));
        }

        return redirect()->route('admin.permissions.index')
            ->with('danger',Lang::get('global.delete_fail'));
    }
}
