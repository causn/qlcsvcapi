<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PermissionUpdateRequest;
use App\Http\Traits\Helpers\ApiResponseTrait;
use App\Repositories\Admin\PermissionRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\Permission;

class PermissionController extends Controller
{
    use ApiResponseTrait;
    protected $permissionRepo;

    public function __construct(
        PermissionRepository $permissionRepository
    )
    {
        $this->authorizeResource(Permission::class,'permission');
        $this->permissionRepo = $permissionRepository;
    }


    public function index(Request $request)
    {
        if(request()->ajax()) {
            $data = $this->permissionRepo->getListWithFilter($request);
            return datatables()->of($data)
                ->addIndexColumn()
                ->addColumn('action',function ($permission){
                    return view('admin.permissions.action',['permission'=>$permission]);
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.permissions.index');
    }

    public function create(Request $request)
    {
        return view('admin.permissions.create');
    }

    public function store(Request $request)
    {
        if($this->permissionRepo->create($request)) {
            return redirect()->route('admin.permissions.index')
                ->with('success', Lang::get('global.permission_created_successfully'));
        }

        return redirect()->back()->with('danger',Lang::get('global.create_fail'));
    }

    public function show(Permission $permission)
    {
        return view('admin.permissions.show',compact('permission'));
    }

    public function edit(Permission $permission)
    {
        return view('admin.permissions.edit',compact('permission'));
    }
    public function update(PermissionUpdateRequest $request, Permission $permission)
    {
        if ($this->permissionRepo->update($permission, $request)) {
            return redirect()->route('admin.permissions.index')
                ->with('success',Lang::get('global.permission_updated_successfully'));
        }

        return redirect()->back()->with('danger',Lang::get('global.update_fail'));
    }

    public function destroy(Permission $permission)
    {
        if($this->permissionRepo->destroy($permission)){
            return redirect()->route('admin.permissions.index')
                ->with('success',Lang::get('global.permission_deleted_successfully'));
        }

        return redirect()->route('admin.permissions.index')
            ->with('danger',Lang::get('global.delete_fail'));
    }
}
