<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Traits\Helpers\ApiResponseTrait;
use App\Models\User;
use App\Repositories\Admin\RoleRepository;
use App\Repositories\Admin\UniversityRepository;
use App\Repositories\Admin\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Laravel\Sanctum\PersonalAccessToken;

class UserController extends Controller
{
    use ApiResponseTrait;
    protected $userRepo;
    protected $roleRepo;
    protected $universityRepo;

    public function __construct(Request $request,
        UserRepository $userRepository,
        RoleRepository $roleRepository,
        UniversityRepository $universityRepository
    )
    {
        parent::__construct($request);
        dd($this->user->u);
        $this->authorizeResource(User::class,'user');
        $this->userRepo = $userRepository;
        $this->roleRepo = $roleRepository;
        $this->universityRepo = $universityRepository;
    }

    public function index(Request $request)
    {
        dd(\auth());
        $jsonResource = new JsonResource([
            'data' => $this->userRepo->getListWithFilterNotMe($request)
        ]);
        return $this->respondWithResource($jsonResource);
    }

    public function create(Request $request)
    {
        $roles = $this->roleRepo->getListWithFilterLess($request);

        if(auth()->user()->hasRole(['administrator'])) {
            $universities = $this->universityRepo->getListWithFilter($request);
            return view('admin.users.create',compact('roles','universities'));
        }

        return view('admin.users.create',compact('roles'));
    }

    public function store(UserCreateRequest $request)
    {
        if($this->userRepo->create($request)) {
            return redirect()->route('admin.users.index')
                ->with('success', Lang::get('global.user_created_successfully'));
        }

        return redirect()->back()->with('danger',Lang::get('global.create_fail'));
    }

    public function edit(Request $request,User $user)
    {
        $avatar = $user->getFirstMedia('avatar');
        if($avatar) {
            $avatar->thumb_url = $avatar->getUrl('thumb');
        }

        $view = 'admin.users.edit';
        if($user->id == auth()->user()->id){
            $view = 'admin.users.profile';
        }
        $roles = $this->roleRepo->getListWithFilterLess($request);

        if(auth()->user()->hasRole(['administrator'])) {
            $universities = $this->universityRepo->getListWithFilter($request);
            return view($view,compact('user','avatar','roles','universities'));
        }

        return view($view,compact('user','avatar','roles'));
    }


    public function update(UserUpdateRequest $request,User $user)
    {
        $route = 'admin.users.index';
        if($user->id == auth()->user()->id){
            $route = 'admin.home';
        }

        if ($this->userRepo->update($user, $request)) {
            return redirect()->route($route)
            ->with('success',Lang::get('global.user_updated_successfully'));
        }

        return redirect()->back()->with('danger',Lang::get('global.update_fail'));

    }

    public function destroy(User $user)
    {
        if($this->userRepo->destroy($user)){
            return redirect()->route('admin.users.index')
            ->with('success',Lang::get('global.user_deleted_successfully'));
        }

        return redirect()->route('admin.users.index')
            ->with('danger',Lang::get('global.delete_fail'));
    }

}
