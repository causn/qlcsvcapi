<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use App\Http\Traits\Helpers\ApiResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MediaController extends Controller
{
    use ApiResponseTrait;
    public function storeMedia(Request $request)
    {
        try{
            $path = storage_path('tmp/uploads');

            if(!file_exists($path)){
                mkdir($path, 0777, true);
            }

            $file = null;
            if($request->filled('name')){
                $name = $request->get('name');
                if($request->hasFile($name)) {
                    $file = $request->file($name);
                }
            }else{
                if($request->hasFile('file')) {
                    $file = $request->file('file');
                }
            }

            if(!$file){
                return $this->respondNotFound();
            }

            $name = uniqid() . '_' . trim($file->getClientOriginalName());
            $file->move($path, $name);

            $jsonResource = new JsonResource([
                'data' => [
                    'name'          => $name,
                    'original_name' => $file->getClientOriginalName(),
                ]
            ]);
            return $this->respondWithResource($jsonResource);
        }catch (\Exception $exception){
            return $this->respondError("error upload",500,$exception);
        }
    }
}
