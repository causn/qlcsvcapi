<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use App\Http\Resources\UniversityResource;
use App\Http\Traits\Helpers\ApiResponseTrait;
use App\Repositories\UniversityRepository;
use Illuminate\Http\Request;

class UniversityController extends Controller
{
    use ApiResponseTrait;
    protected $universityRepo;
    public function __construct(UniversityRepository $universityRepository)
    {
        $this->universityRepo = $universityRepository;
    }

    public function detail(Request $request,$id){
        if(!$id){
            return $this->respondNotFound();
        }

        $university = $this->universityRepo->findId($id);
        if(empty($university)){
            return $this->respondNotFound();
        }

        return $this->respondWithResource(new UniversityResource($university->toArray()));
    }

    public function list(Request $request){
        $universities = $this->universityRepo->getList($request);
        return $this->respondWithResource(new UniversityResource($universities));
    }
}
