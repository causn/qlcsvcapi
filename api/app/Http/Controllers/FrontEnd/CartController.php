<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use App\Http\Traits\Helpers\ApiResponseTrait;
use App\Repositories\AssetRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{
    use ApiResponseTrait;
    protected $assetRepo;

    public function __construct(AssetRepository $assetRepository)
    {
       $this->assetRepo = $assetRepository;
    }

    public function list(Request $request)
    {
        if(auth()->guest()){
            return $this->respondForbidden();
        }

        $jsonResource = new JsonResource([
            'data' => session()->get('cart',[])
        ]);
        return $this->respondWithResource($jsonResource);
    }

    public function update(Request $request){
        if(auth()->guest()){
            return $this->respondForbidden();
        }

        $validator = Validator::make($request->all(), [
            'id' => ['required','numeric'],
            'qty' => ['required','numeric','min:0','min:0'],
            'start' => ['required',function ($attribute, $value, $fail) {
                if(!empty($value)){
                    try{
                        Carbon::createFromFormat('d/m/Y', $value);
                    }catch (\Exception $exception){
                        $fail(Lang::get('validation.regex',['attribute'=>Lang::get('global.birthday')]));
                    }
                }
            }],
            'end' => ['required',function ($attribute, $value, $fail) {
                if(!empty($value)){
                    try{
                        Carbon::createFromFormat('d/m/Y', $value);
                    }catch (\Exception $exception){
                        $fail(Lang::get('validation.regex',['attribute'=>Lang::get('global.birthday')]));
                    }
                }
            }],
        ]);

        if($validator->fails()){
            return $this->respondValidationErrors($validator);
        }

        $id = (int)$request->get('id');
        $asset = $this->assetRepo->findById((int)$request->get('id'));

        if(!$asset){
            return $this->respondNotFound();
        }

        $cart = session()->get('cart', []);
        //remove
        $qty = $request->get('qty');
        if($qty <= 0){
            if(isset($cart[$id])){
                unset($cart[$id]);
                session()->put('cart', $cart);
                $jsonResource = new JsonResource([
                    'data' => $cart
                ]);
                return $this->respondWithResource($jsonResource);
            }
        }

        $startDate = $request->get('start');
        $endDate = $request->get('end');

        if(!isset($cart[$id])){
            $university = $asset->university()->first();
            $cart[$id] = [
                "name" => $asset->name,
                'university' => $university->name,
                'university_id' => $university->id,
                "quantity" => $qty,
                "price" => $asset->price,
                "start_date" => $startDate,
                "end_date" => $endDate,
            ];
            session()->put('cart', $cart);
            $jsonResource = new JsonResource([
                'data' => $cart
            ]);
            return $this->respondWithResource($jsonResource);
        }

        $cart[$id]['quantity'] = $qty;
        $cart[$id]['start_date'] = $startDate;
        $cart[$id]['end_date'] = $endDate;
        session()->put('cart', $cart);

        $jsonResource = new JsonResource([
            'data' => $cart
        ]);
        return $this->respondWithResource($jsonResource);
    }
}
