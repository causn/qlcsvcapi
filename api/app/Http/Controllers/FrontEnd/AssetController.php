<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use App\Http\Traits\Helpers\ApiResponseTrait;
use App\Models\Asset;
use App\Repositories\AssetRepository;
use App\Repositories\RentRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class AssetController  extends Controller
{
    use ApiResponseTrait;
    protected $assetRepo;
    protected $rentRepo;
    protected $perPage = 9;
    public function __construct(
        AssetRepository $assetRepository,
        RentRepository  $rentRepository
    )
    {
        $this->assetRepo = $assetRepository;
        $this->rentRepo = $rentRepository;
    }

    public function detail(Request $request, $id){
        if(!$id){
            return  $this->respondNotFound();
        }

        $asset = $this->assetRepo->findById($id);
        if(!$asset){
            return  $this->respondNotFound();
        }

        if($asset->status != Asset::APPROVED_STATUS || $asset->number <= 0){
            return $this->respondForbidden();
        }

        $jsonResource = new JsonResource([
            'data' => $asset
        ]);
        return $this->respondWithResource($jsonResource);
    }

    public function checkActive(Request $request){
        $validator = Validator::make($request->all(), [
            'id'=> ['required','numeric'],
            'qty' => ['required','numeric','min:0','min:0'],
            'start' => ['required',function ($attribute, $value, $fail) {
                if(!empty($value)){
                    try{
                        Carbon::createFromFormat('d/m/Y', $value);
                    }catch (\Exception $exception){
                        $fail(Lang::get('validation.regex',['attribute'=>Lang::get('global.birthday')]));
                    }
                }
            }],
            'end' => ['required',function ($attribute, $value, $fail) {
                if(!empty($value)){
                    try{
                        Carbon::createFromFormat('d/m/Y', $value);
                    }catch (\Exception $exception){
                        $fail(Lang::get('validation.regex',['attribute'=>Lang::get('global.birthday')]));
                    }
                }
            }],
        ]);

        if($validator->fails()) {
            return $this->respondValidationErrors($validator);
        }

        if(!$this->assetRepo->checkActive((int)$request->get('id'),$request)){
            return $this->respondNotFound();
        }

        return $this->respondSuccess();
    }
    public function rent(Request $request){
        if(auth()->guest()){
            abort(404);
        }

        $result = $this->rentRepo->create($request);
        if(!$result['status']){
            return redirect()->back()->with('danger',Lang::get('global.'.$result['message']));
        }
        session()->forget(['cart']);
        return view('asset.thank');
    }
    public function list(Request $request){
        $assets = $this->assetRepo->getListActive($request,[],$this->perPage);
        $jsonResource = new JsonResource([
            'data' => $assets
        ]);
        return $this->respondWithResource($jsonResource);
    }
}
