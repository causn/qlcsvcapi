<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use App\Models\University;
use App\Repositories\AssetRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Api\ApiController;

class HomeController extends Controller
{
    protected $assetRepo;

    public function __construct(AssetRepository $assetRepository)
    {
        $this->assetRepo = $assetRepository;
    }

    public function index(Request $request){
        $universities = University::query()->get();

        return view('home',[
            'universities' => $universities
        ]);
    }

    public function s3(Request  $request){
        $disk = 's3';
        $heroImage = base64_encode(file_get_contents(public_path().'/storage/'."hero.jpg"));
        if($heroImage) {
            $uploadedPath = Storage::disk($disk)->put('hero.jpg', $heroImage);
            return Storage::disk($disk)->url($uploadedPath);
        }
        return  'ssss';
    }

    public function about(Request  $request){
        return view('about');
    }
}
