<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use App\Http\Requests\FeProfileUpdateRequest;
use App\Http\Traits\Helpers\ApiResponseTrait;
use App\Repositories\RentRepository;
use App\Repositories\UserRepository;
use App\Rules\PhoneNumber;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    use ApiResponseTrait;
    protected $rentRepo;
    protected $userRepo;
    public function __construct(
        RentRepository $rentRepository,
        UserRepository $userRepository
    )
    {
        $this->rentRepo = $rentRepository;
        $this->userRepo = $userRepository;
    }
    public function index(){
        if(auth()->guest()){
            return $this->respondForbidden();
        }

        $jsonResource = new JsonResource([
            'data' => auth()->user()
        ]);
        return $this->respondWithResource($jsonResource);
    }

    public function rentStatus(){
        if(auth()->guest()){
            return $this->respondForbidden();
        }

        $jsonResource = new JsonResource([
            'data' => $this->rentRepo->getListByCurrentAuth()
        ]);
        return $this->respondWithResource($jsonResource);
    }

    public function update(FeProfileUpdateRequest $request)
    {
        if(auth()->guest()){
            return $this->respondForbidden();
        }

        $validator = Validator::make($request->all(), [
            'password' => ['same:password_confirmation'],
            'phone' => [new PhoneNumber()],
            'birthday'=>[
                function ($attribute, $value, $fail) {
                    if(!empty($value)){
                        try{
                            Carbon::createFromFormat('d/m/Y', $value);
                        }catch (\Exception $exception){
                            $fail(Lang::get('validation.regex',['attribute'=>Lang::get('global.birthday')]));
                        }
                    }
                }
            ]
        ]);

        if($validator->fails()) {
            return $this->respondValidationErrors($validator);
        }

        $request->merge(['id'=>auth()->user()->id]);

        if (!$this->userRepo->update($request)) {
            return $this->respondInternalError();
        }

        $jsonResource = new JsonResource([
            'data' => auth()->user()
        ]);
        return $this->respondWithResource($jsonResource);
    }
}
