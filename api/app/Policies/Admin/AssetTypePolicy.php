<?php

namespace App\Policies\Admin;

use App\Models\Admin\AssetType;
use App\Models\Admin\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AssetTypePolicy
{
    use HandlesAuthorization;

    public function index(User $user){
        return $user->hasPermissionTo('asset-type-list');
    }

    public function update(User $user,AssetType $assetType = null){
        return $user->hasPermissionTo('asset-type-edit');
    }

    public function store(User $user,AssetType $assetType = null){
        return $user->hasPermissionTo('asset-type-create');
    }

    public function destroy(User $user,AssetType $assetType = null){
        return $user->hasPermissionTo('asset-attribute-delete');
    }
}
