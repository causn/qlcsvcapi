<?php

namespace App\Policies\Admin;

use App\Models\Liquidation;
use App\Models\Admin\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LiquidationPolicy
{
    use HandlesAuthorization;

    public function index(User $user): bool
    {
        return $user->hasPermissionTo('liquidation-list');
    }

    public function store(User $user,Liquidation $liquidation = null): bool
    {
        return true;
    }

    public function update(User $user, Liquidation $liquidation = null): bool
    {
        $status = false;

        switch($liquidation->status){
            case Liquidation::DRAFT_STATUS:
                $status = true;
                break;
            case Liquidation::APPROVED_STATUS:
                if($user->hasRole(['admin','director','manager'])){
                    $status = true;
                }
                break;
            default:
            $status = false;
            break;
        }
        return $status;
    }

    public function approval(User $user,Liquidation $liquidation = null){
        if($user->hasRole(['administrator','admin','director','manager']) && $liquidation->status == Liquidation::DRAFT_STATUS){
            return true;
        }

        return false;
    }

    public function destroy(User $user, Liquidation $liquidation): bool
    {
        if($user->hasRole(['administrator','admin','director','manager']) && $liquidation->status != Liquidation::CANCEL_STATUS){
            return true;
        }

        if($user->hasRole(['staff']) && $liquidation->status == Liquidation::DRAFT_STATUS){
            return true;
        }

        return  false;
    }
}
