<?php

namespace App\Policies\Admin;

use App\Models\Admin\University;
use App\Models\Admin\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UniversityPolicy
{
    use HandlesAuthorization;

    public function index(User $user): bool
    {
        return $user->hasPermissionTo('university-list');
    }

    public function store(User $user): bool
    {
        return $user->hasPermissionTo('university-create');
    }

    public function update(User $user, University $university): bool
    {
        return $user->hasPermissionTo('university-edit');
    }

    public function delete(User $user,University $university): bool
    {
        return $user->hasPermissionTo('university-delete');
    }
}
