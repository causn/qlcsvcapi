<?php

namespace App\Policies\Admin;

use App\Models\Admin\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use function Termwind\renderUsing;

class UserPolicy
{
    use HandlesAuthorization;

    public function index(User $user): bool
    {
        return $user->hasPermissionTo('user-list','admin');
    }

    public function store(User $user): bool
    {
        return $user->hasPermissionTo('user-create','admin');
    }

    public function update(User $user, User $userModel): bool
    {
        if($user->id === $userModel->id){
            return  true;
        }else{
            if($user->hasPermissionTo('user-edit','admin') && ($user->level < $userModel->level || $user->id === $userModel->id ) ){
                return true;
            }
        }

        return  false;
    }

    public function destroy(User $user, User $userModel): bool
    {
        if($user->hasPermissionTo('user-delete','admin') && ($user->level < $userModel->level || $user->id === $userModel->id ) ){
            return true;
        }

        return  false;
    }

    public function profile(User $user, User $userModel): bool
    {
        if($user->hasPermissionTo('profile','admin') && $user->id === $userModel->id){
            return true;
        }

        return  false;
    }

    public function backup(User $user){
        if($user->hasRole(['administrator'])){
            return true;
        }
        return false;
    }
}
