<?php

namespace App\Policies\Admin;

use App\Models\Permission;
use App\Models\Admin\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermissionPolicy
{
    use HandlesAuthorization;

    public function index(User $user): bool
    {
        return $user->hasPermissionTo('permission-list');
    }

    public function store(User $user): bool
    {
        return $user->hasPermissionTo('permission-create');
    }

    public function update(User $user,Permission $permission): bool
    {
        return $user->hasPermissionTo('permission-edit');
    }

    public function delete(User $user,Permission $permission): bool
    {
        return $user->hasPermissionTo('permission-delete');
    }
}
