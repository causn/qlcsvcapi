<?php

namespace App\Policies\Admin;

use App\Models\Admin\Asset;
use App\Models\Admin\User;
use App\Repositories\Admin\AssetRepository;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Http\Request;

class AssetPolicy
{
    use HandlesAuthorization;
    protected $assetRepo;

    public function __construct(AssetRepository $assetRepository)
    {
        $this->assetRepo = $assetRepository;
    }

    public function index(User $user){
        return $user->can('asset-list');
    }

    public function show(User $user){
        return $user->can('asset-list');
    }

    public function update(User $user,Asset $asset){
        try {
            if($asset->status == Asset::DRAFT_STATUS){
                return true;
            }else{
                if($user->hasRole(['administrator', 'admin', 'director', 'manager'])){
                    return  true;
                }
                return false;
            }
        }
        catch (\Exception $exception){
            return  false;
        }
    }

    public function create(User $user){
       return $user->can('asset-create');
    }
    public function store(User $user,Asset $asset = null){
        return $user->can('asset-create');
    }

    public function destroy(User $user,Asset $asset){
        try {
            if ($this->checkActive($asset)) {
                if ($user->hasRole(['administrator', 'admin', 'director', 'manager'])) {
                    return true;
                }

                if ($user->hasRole(['staff'])) {
                    if ($asset->status == Asset::DRAFT_STATUS) {
                        return true;
                    }
                }

                return false;
            }
            return false;
        }
        catch (\Exception $exception){
            return false;
        }
    }

    public function approval(User $user,Asset $asset){
        if($user->hasRole(['administrator', 'admin', 'director', 'manager'])){
            return  true;
        }else{
            return false;
        }
    }

    public function approval_create(User $user,Asset $asset){
        if($this->checkActive($asset)) {
            if ($user->hasRole(['administrator', 'admin', 'director', 'manager'])) {
                return true;
            }

            return false;
        }
        return false;
    }

    private function checkActive(Asset $asset){
        $request = new Request();
        $request->merge(['id'=>$asset->id]);
        return $this->assetRepo->getActive($request,[],1);
    }
}
