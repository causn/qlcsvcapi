<?php

namespace App\Policies\Admin;

use App\Models\Admin\AssetAttribute;
use App\Models\Admin\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AssetAttributePolicy
{
    use HandlesAuthorization;

    public function index(User $user){
        return $user->hasPermissionTo('asset-attribute-list');
    }

    public function update(User $user,AssetAttribute $assetAttribute){
        return $user->hasPermissionTo('asset-attribute-edit');
    }

    public function store(User $user){
        return $user->hasPermissionTo('asset-attribute-create');
    }

    public function destroy(User $user,AssetAttribute $assetAttribute){
        return $user->hasPermissionTo('asset-attribute-delete');
    }
}
