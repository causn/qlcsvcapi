<?php

namespace App\Policies\Admin;

use App\Models\Admin\Company;
use App\Models\Admin\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CompanyPolicy
{
    use HandlesAuthorization;

    public function index(User $user){
        return $user->hasPermissionTo('company-list');
    }

    public function update(User $user,Company $company){
        return $user->hasPermissionTo('company-edit');
    }

    public function store(User $user){
        return $user->hasPermissionTo('company-create');
    }

    public function destroy(User $user,Company $company){
        return $user->hasPermissionTo('company-delete');
    }
}
