<?php

namespace App\Policies\Admin;

use App\Models\Admin\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    public function index(User $user): bool
    {
        return $user->hasPermissionTo('role-list');
    }

    public function store(User $user): bool
    {
        return $user->hasPermissionTo('role-create');
    }

    public function update(User $user): bool
    {
        return $user->hasPermissionTo('role-edit');
    }

    public function delete(User $user): bool
    {
        return $user->hasPermissionTo('role-delete');
    }
}
