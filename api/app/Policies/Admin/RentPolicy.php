<?php

namespace App\Policies\Admin;

use App\Models\Rent;
use App\Models\Admin\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RentPolicy
{
    use HandlesAuthorization;

    public function index(User $user): bool
    {
        return $user->hasPermissionTo('rent-list');
    }

    public function store(User $user,Rent $rent = null): bool
    {
        return true;
    }

    public function update(User $user, Rent $rent = null): bool
    {
        $status = false;

        switch($rent->status){
            case Rent::DRAFT_STATUS:
                $status = true;
                break;
            case Rent::APPROVED_STATUS:
                if($user->hasRole(['admin','director','manager'])){
                    $status = true;
                }
                break;
            default:
            $status = false;
            break;
        }
        return $status;
    }

    public function approval(User $user,Rent $rent = null){
        if($user->hasRole(['administrator','admin','director','manager']) && $rent->status == Rent::DRAFT_STATUS){
            return true;
        }

        return false;
    }

    public function destroy(User $user, Rent $rent): bool
    {
        if($user->hasRole(['administrator','admin','director','manager']) && $rent->status != Rent::CANCEL_STATUS){
            return true;
        }

        if($user->hasRole(['staff']) && $rent->status == Rent::DRAFT_STATUS){
            return true;
        }

        return  false;
    }
}
