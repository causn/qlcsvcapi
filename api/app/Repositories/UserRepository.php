<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserRepository
{
    public function update(Request $request)
    {
        DB::beginTransaction();
        try {
            $model = $request->filled('id') ? User::query()->where('id',$request->get('id'))->first() : null;
            if(!$model){
                return false;
            }

            $attributes = $request->only((new User())->getFillable() ?? []);

            if (!empty($attributes['password'])) {
                $attributes['password'] = Hash::make($attributes['password']);
            } else {
                $attributes = Arr::except($attributes, array('password'));
            }

            $model->update($attributes);

            if($request->filled('avatar')) {
                $avatar = $model->getFirstMedia('avatar');
                if($avatar) {
                    $avatar->delete();
                }
                $fileArr = json_decode($request->get('avatar'));
                $model->addMedia(storage_path('tmp/uploads/' . $fileArr->name))->toMediaCollection('avatar');
            }

            DB::commit();
            return $model;
        }
        catch (\Exception $exception){
            DB::rollBack();
            return  false;
        }
    }
}
