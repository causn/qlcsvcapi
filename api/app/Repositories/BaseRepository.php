<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

abstract class BaseRepository implements RepositoryInterface
{
    /** @var Model $model */
    protected $model = User::class;
    protected $defaultOrder = ['created_at'=>'desc','id'=>'desc'];

    public function getQueryWithFilter($attributes = [],$orders = []){
        /** @var Builder $query */
        $query = $this->model::query();
        $query = $this->setBaseCriteria($query);
        $query = $this->setBaseOrder($query);

        $query = $this->setCustomCondition($query,$attributes);
        if(method_exists($this->model,'withAll')){
            $query->withAll();
        }

        return $this->setCustomOrder($query,$orders);
    }

    public function getListWithFilter(Request $request,$orders = [],$limit = null){
        $attributes = $request->only((new $this->model)->getFillable() ?? []);
        $query = $this->getQueryWithFilter($attributes,$orders);
        return $this->getResult($query,$limit);
    }

    public function find($id)
    {
        return $this->model::find($id);
    }

    public function create(Request $request)
    {
        $attributes = $request->only((new $this->model)->getFillable() ?? []);

        return $this->model::create($attributes);
    }

    public function update(Model $model,Request $request)
    {
        try{
            $attributes = $request->only((new $this->model)->getFillable() ?? []);
            $model->fill($attributes);
            $model->save();
            return $model;
        }catch(\Exception $exception){
            return false;
        }
    }

    public function setBaseCriteriaRole(Builder $query){
        /** @var User $user */
        $user = auth()->user();

        if($user->hasRole(config('permission.role_is_supper_admin_name'))){
            return $query;
        }

        return  $query;
    }

    public function setBaseUniversity(Builder $query){
        /** @var User $user */
        $user = auth()->user();

        $universityId = $user->university()->first()->id ?? null;
        if(!empty($universityId)){
            $query->whereHas('university',function($q) use ($universityId){
                $q->where('university_id',$universityId);
            });
        }

        return  $query;
    }

    public function setBaseCriteria(Builder $query){
        $query = $this->setBaseCriteriaRole($query);
        $query = $this->setBaseUniversity($query);
        return  $query;
    }

    public function setBaseOrder(Builder $query){
        foreach ($this->defaultOrder as $key => $order){
            $query->orderBy($key,$order);
        }
        return $query;
    }

    public function findOrFail($id,$attributes = []){
        return $this->model::findOrFail($id)->fill($attributes);
    }

    public function destroy(Model $model)
    {
        return $model->delete();
    }

    public function setCustomOrder(Builder $query, $orders = []){
        if(is_array($orders) && count($orders) > 0){
            foreach ($orders as $key => $order){
                $query->orderBy($key,$order);
            }
        }
        return $query;
    }

    public function setCustomCondition(Builder $query, $conditions = []){
        if(is_array($conditions) && count($conditions) > 0){
            foreach ($conditions as $key => $condition){
                if(is_array($condition)){
                    foreach ($condition as $k => $at){
                        if($at) {
                            switch ($k) {
                                case "in":
                                    $query->whereIn($key, $at);
                                    break;
                                case "not_in":
                                    $query->whereNotIn($key, $at);
                                    break;
                                default:
                                    $query->where($key, $k, $at);
                                    break;
                            }
                        }
                    }
                }else {
                    $query->where($key,$condition);
                }
            }
        }
        return $query;
    }

    public function getResult(Builder $query, $limit = null){
        if($limit){
            if($limit != 1){
                $query->limit($limit);
            }else{
                return $query->first();
            }
        }

        return  $query->get();
    }

    public function cursorPaginate(Request $request,$orders = [],$limit = null)
    {
        $attributes = $request->only((new $this->model)->getFillable() ?? []);

        if(is_null($limit)){
            if($request->filled('per_page')){
                $limit = (int)$request->get('per_page');
            }else {
                $limit = config('paginate', 10);
            }
        }

        $query = $this->getQueryWithFilter($attributes,$orders);

        return $query->paginate($limit);
    }
}
