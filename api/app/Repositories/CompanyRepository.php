<?php

namespace App\Repositories;

use App\Models\Company;
use Illuminate\Http\Request;

class CompanyRepository
{
    public function getListUniversity(Request  $request){
        $companies = Company::all();
        $list = [];

        foreach ($companies as $company){
            $university = $company->university()->first();
            if(!isset($list[$university->id]['entity'])){
                $list[$university->id]['entity'] = $university;
            }
            if(!isset($list[$university->id]['items'][$company->id])){
                $list[$university->id]['items'][$company->id] = $company;
            }
        }

        return $list;
    }
}
