<?php

namespace App\Repositories;

use App\Models\Asset;
use App\Models\Rent;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class RentRepository
{
    public function create(Request $request)
    {
        DB::beginTransaction();
        try {
            $cart = session()->get('cart', []);
            $listRent = [];
            foreach ($cart as $key => $c) {
                $asset = Asset::query()->where('id', $key)->first();

                $attributes = [
                    'start_time' => Carbon::createFromFormat('d/m/Y', $c['start_date'])->format('Y-m-d 00:00:00'),
                    'end_time' => Carbon::createFromFormat('d/m/Y', $c['end_date'])->format('Y-m-d 23:59:59'),
                    'status' => Asset::DRAFT_STATUS,
                    'qty' => (int)$c['quantity'],
                    'asset_code' => $asset->code,
                    'create_by' => auth()->user()->id,
                    'update_by' => auth()->user()->id
                ];

                $rent = Rent::create($attributes);
                $rent->university()->detach();
                $rent->university()->attach($c['university_id']);

                $listRent[] = $rent;
            }

            DB::commit();

            return [
                'status' => true,
                'message' => 'Success',
                'data' => $listRent
            ];
        } catch (\Exception $exception) {
            Log::error("User: " . auth()->user()->email . " rent frontend error with message:" . $exception->getMessage());
            DB::rollBack();
            return [
                'status' => false,
                'message' => 'An error occurred when creating a rental order. Please contact the administrator for processing'
            ];
        }
    }

    public function getListByCurrentAuth(){
        $user =  auth()->user();
        return Rent::query()->where('create_by',$user->id)->get();
    }
}
