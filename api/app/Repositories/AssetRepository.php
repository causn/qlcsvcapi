<?php

namespace App\Repositories;

use App\Models\Asset;
use App\Models\Rent;
use Carbon\Carbon;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AssetRepository
{
    public function getListActive(Request $request, $orderBy = [], $limit = null){
        $query = Asset::query();
        $query->where('status',Asset::APPROVED_STATUS);

        $startDate = Carbon::now();
        if($request->filled('start_date')){
            $startDate = $request->get('start_date');
        }

        $endDate = Carbon::now();
        if($request->filled('end_date')){
            $endDate = $request->get('end_date');
        }

        $universities = null;
        if($request->filled('universities')){
            $universities = $request->get('universities');
        }

        $query->whereDoesntHave('liquidations', function ($q) use ($startDate,$endDate) {
            $q->where('status','!=',Rent::DRAFT_STATUS)
                ->where('end_time', '<', $endDate)
                ->orWhere('start_time', '>', $startDate);
        });

        $query->whereDoesntHave('rent', function ($q) use ($startDate,$endDate){
            $q->where('status','!=',Rent::DRAFT_STATUS)
                ->where('end_time', '<', $endDate)
                ->orWhere('start_time', '>', $startDate);
        });

        if($universities){
            $query->whereHas('university',function ($q) use ($universities){
                $q->whereIn('universities.id',$universities);
            });
        }

        if(empty($orderBy)){
            $orderBy = ['created_at'=>'desc','updated_at'=>'desc','id'=>'desc'];
        }

        foreach ($orderBy as $key => $order){
            $query->orderBy($key,$order);
        }

        if($limit){
            $page = 1;
            if($request->filled('page')){
                $page = (int)$request->get('page');
            }
            return $query->paginate($limit,['*'],'page',$page);
        }

        return  $query->get();
    }

    public function findById($id){
        return Asset::query()->where('id',$id)->with(['media'])->first();
    }

    public function checkActive($id, Request $request){

        $startDate = $request->filled('start_date') ? Carbon::createFromFormat('d/m/Y',$request->query('start_date'))->format('Y-m-d') : null;
        $endDate = $request->filled('end_date') ? Carbon::createFromFormat('d/m/Y',$request->query('end_date'))->format('Y-m-d') : null;
        $qty = $request->filled('qty') ? (int)$request->get('qty') : 1;

        $result = DB::table('assets')
            ->select(DB::raw('COALESCE(SUM(`asset_stock`.`number`),0) AS `used`, `assets`.`number` as `total` '))
            ->leftJoin('asset_stock', function (JoinClause $join) use ($startDate,$endDate) {
                $join->on('assets.id', '=', 'asset_stock.asset_id');
                if($startDate) {
                    $join->where('asset_stock.start_date', '>=', $startDate);
                }
                if($endDate) {
                    $join->where('asset_stock.end_date', '<=', $endDate);
                }
            })
            ->where('assets.status',Asset::APPROVED_STATUS)
            ->where('assets.id',$id)
            ->first();

        $total = (int)$result->total;
        $used = (int)$result->used;
        if($total == 0 || $total == $used || $qty < 1 || ( ($total-$used) < $qty)){
            return false;
        }

        $asset = Asset::query()->where('id',$id)->first();

        $asset->used = $result->used;
        $asset->total = $result->total;

        return  $asset;
    }
}
