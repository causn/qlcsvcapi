<?php

namespace App\Repositories\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;
use Spatie\DbDumper\Databases\MySql;

class SystemDatabaseRepository
{
    const STATUS_DELETED = 0;
    const STATUS_CREATED = 1;
    const STATUS_CURRENT = 2;
    protected $pathList;
    protected $uersRepo;
    public function __construct(UserRepository  $userRepository)
    {
        $this->pathList = public_path('backup-restore') . '/data.json';
        $this->checkFileList();
        $this->uersRepo = $userRepository;
    }

    public function getListWithFilter(Request $request)
    {
        $arr = json_decode(File::get($this->pathList),true) ?? [];

        foreach ($arr as $key => &$value){
            if(!empty($value['create_by'])) {
                $value['createBy'] = $this->uersRepo->find($value['create_by'])->toArray();
                $value['create_by_name'] = $value['createBy']['name'];
            }
            if(!empty($value['delete_by'])) {
                $value['deleteBy'] = $this->uersRepo->find($value['delete_by'])->toArray();
                $value['delete_by_name'] = $value['deleteBy']['name'];
            }
        }

        return $arr;
    }

    public function restore(Request $request,$name,$fileName){
        $status = [
            'status' => false,
            'message' => ''
        ];

        $pathFileBackup = public_path('backup-restore') . '/' . $fileName;

        if (!File::exists($pathFileBackup)) {
            $status['message'] = Lang::get("global.file doesn't exist");
            return $status;
        }

        $json = File::get($this->pathList);
        $array = json_decode($json, true) ?? [];

        $sql = File::get($pathFileBackup);

        if(!isset($array[$name])){
            $array[$name] = [
                'name' => $name,
                'mime' => '.sql',
                'file_name' => $name . '.sql',
                'file_path' => $pathFileBackup,
                'create_by' => auth()->user()->id,
                'created' => date_format(now(), 'Y-m-d H:i:s'),
                'delete_by' => null,
                'deleted' => null
            ];
        }

        $restore = $array[$name]['restore'] ?? [];
        $restore[] = [
            'user_id' => auth()->user()->id,
            'time' => date_format(now(), 'Y-m-d H:i:s')
        ];

        $array[$name]['restore'] = $restore;
        $array[$name]['status'] = self::STATUS_CURRENT;

        foreach ($array as $key => &$item){
            if($key != $name && $item['status'] == self::STATUS_CURRENT){
                $item['status'] = self::STATUS_CREATED;
            }
        }

        $json = json_encode($array, true);

        if (!File::put($this->pathList, $json)) {
            $status['message'] = Lang::get("global.restore fail");
            return $status;
        }

        DB::unprepared($sql);

        return true;
    }

    public function delete(Request $request,$name,$fileName)
    {
        $status = [
            'status' => false,
            'message' => ''
        ];
        try {
            $pathFileBackup = public_path('backup-restore') . '/' . $fileName;

            if (!File::exists($pathFileBackup)) {
                $status['message'] = Lang::get("global.file doesn't exist");
                return $status;
            }

            if (!File::delete($pathFileBackup)) {
                $status['message'] = Lang::get("global.delete_fail");
                return $status;
            }

            $json = File::get($this->pathList);
            $array = json_decode($json, true) ?? [];

            if (empty($array[$request->get('name')])) {
                return false;
            }

            $array[$name]['delete_by'] = auth()->user()->id;
            $array[$name]['deleted'] = date_format(now(), 'Y-m-d H:i:s');
            $array[$name]['status'] = self::STATUS_DELETED;

            $json = json_encode($array, true);

            if (!File::put($this->pathList, $json)) {
                $status['message'] = Lang::get("global.delete_fail");
                return $status;
            }

            $status['message'] = Lang::get('global.success');
            $status['status'] = true;

            return $status;
        } catch (\Exception $exception) {
            $status['message'] = Lang::get('global.error by system');
            return $status;
        }
    }

    public function backup(Request $request)
    {
        $status = [
            'status' => false,
            'message' => ''
        ];
        try {
            $name = Str::uuid()->toString();
            $pathFileBackup = public_path('backup-restore') . '/' . $name . '.sql';

            $json = File::get($this->pathList);

            $array = json_decode($json, true) ?? [];

            MySql::create()
                ->setDbName('qlcsvc')
                ->setUserName('root')
                ->dumpToFile($pathFileBackup);

            $array[$name] = [
                'name' => $name,
                'mime' => '.sql',
                'file_name' => $name . '.sql',
                'file_path' => $pathFileBackup,
                'create_by' => auth()->user()->id,
                'created' => date_format(now(), 'Y-m-d H:i:s'),
                'delete_by' => null,
                'deleted' => null,
                'status' => self::STATUS_CREATED
            ];

            $json = json_encode($array, true);

            if (!File::put($this->pathList, $json)) {
                $status['message'] = Lang::get('global.create file not success');
                return $status;
            }

            $status['message'] = Lang::get('global.success');
            $status['status'] = true;

            return $status;
        } catch (\Exception $exception) {
            $status['message'] = Lang::get('global.error by system');
            return $status;
        }
    }

    private function checkFileList(){
        if (!File::exists($this->pathList)) {
            File::put($this->pathList, null);
        }
    }
}
