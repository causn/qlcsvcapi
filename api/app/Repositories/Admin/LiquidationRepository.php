<?php

namespace App\Repositories\Admin;

use App\Models\Liquidation;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LiquidationRepository extends BaseRepository
{
    protected $model = Liquidation::class;

    public function create(Request $request)
    {
        DB::beginTransaction();
        try {
            $attributes = $request->only((new $this->model)->getFillable() ?? []);
            if(isset($attributes['start_time'])){
                $attributes['start_time'] = Carbon::createFromFormat('d/m/Y',$attributes['start_time']);
            }
            if(isset($attributes['end_time'])){
                $attributes['end_time'] = Carbon::createFromFormat('d/m/Y',$attributes['end_time']);
            }
            $model = $this->model::create($attributes);

            $auth = auth()->user();
            if($auth->hasRole(['administrator'])){
                $universityId = $attributes['university'] ?? null;
            }else{
                $university = auth()->user()->university()->first();
                $universityId = $university->id ?? null;
            }

            if($universityId){
                $model->university()->detach();
                $model->university()->attach($universityId);
            }

            if($request->filled('company')){
                $model->company()->detach();
                $model->company()->attach($request->get('company'));
            }

            DB::commit();
            return $model;
        }catch (\Exception $exception){
            DB::rollBack();
            return false;
        }
    }
    public function update(Model $model,Request $request)
    {
        DB::beginTransaction();
        try {
            $attributes = $request->only((new $this->model)->getFillable() ?? []);
            if(isset($attributes['start_time'])){
                $attributes['start_time'] = Carbon::createFromFormat('d/m/Y',$attributes['start_time']);
            }
            if(isset($attributes['end_time'])){
                $attributes['end_time'] = Carbon::createFromFormat('d/m/Y',$attributes['end_time']);
            }
            foreach ($attributes as $key => $attribute){
                $model->$key = $attribute;
            }

            $auth = auth()->user();
            if($auth->hasRole(['administrator'])){
                $universityId = $attributes['university'] ?? null;
            }else{
                $university = auth()->user()->university()->first();
                $universityId = $university->id ?? null;
            }

            if($universityId){
                $model->university()->detach();
                $model->university()->attach($universityId);
            }

            if($request->filled('company')){
                $model->company()->detach();
                $model->company()->attach($request->get('company'));
            }

            if($model->status == Liquidation::APPROVED_STATUS){
                $asset = $model->asset()->first();
                $number = $asset->number;
                $asset->number = $number - (int)$model->qty;
                $asset->save();
            }

            $model->save();
            DB::commit();
            return $model;
        }catch (\Exception $exception){
            DB::rollBack();
            return false;
        }
    }

    public function formatOutput(Model $model){
        if($model->start_time != '0000-00-00 00:00:00' && !empty($model->start_time)){
            $model->start_time = Carbon::createFromFormat('Y-m-d H:i:s', $model->start_time);
        }else{
            $model->start_time = null;
        }

        if($model->end_time != '0000-00-00 00:00:00' && !empty($model->end_time)){
            $model->end_time = Carbon::createFromFormat('Y-m-d H:i:s', $model->end_time);
        }else{
            $model->end_time = null;
        }
        return $model;
    }

    public function destroy(Model $model)
    {
        if($model->status == Liquidation::DRAFT_STATUS){
            return $model->delete();
        }

        $request = new Request();
        $request->merge(["status"=>Liquidation::CANCEL_STATUS]);

        return $this->update($model,$request);
    }
}
