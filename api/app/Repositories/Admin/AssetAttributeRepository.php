<?php

namespace App\Repositories\Admin;

use App\Models\AssetAttribute;
use App\Repositories\BaseRepository;

class AssetAttributeRepository extends BaseRepository
{
    protected $model = AssetAttribute::class;
}
