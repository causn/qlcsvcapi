<?php

namespace App\Repositories\Admin;

use App\Models\University;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UniversityRepository extends BaseRepository
{
    protected $model = University::class;

    public function setBaseCriteria(Builder $query){
        /** @var User $user */
        $user = auth()->user();

        if($user->hasRole(config('permission.role_is_supper_admin_name'))){
            return $query;
        }

        return  $query;
    }

    public function create(Request $request)
    {
        DB::beginTransaction();
        try {
            $attributes = $request->only((new $this->model)->getFillable() ?? []);

            $model = University::create($attributes);

            foreach ($request->input('main_images', []) as $file) {
                $model->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('main_images');
            }

            foreach ($request->input('logo', []) as $file) {
                $model->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('logo');
            }

            DB::commit();
            return $model;
        }catch (\Exception $exception){
            DB::rollBack();
            return false;
        }
    }

    public function update(Model $model, Request $request)
    {
        DB::beginTransaction();
        try {
            $attributes = $request->only((new $this->model)->getFillable() ?? []);
            $properties = $attributes;
            foreach ($attributes as $key => $attribute){
                $model->$key = $attribute;
            }

            $model->save();

            //update image images
            $mainImages = $model->getMedia('main_images');
            if (count($mainImages) > 0) {
                foreach ($mainImages as $media) {
                    if (!in_array($media->file_name, $request->input('main_images', []))) {
                        $media->delete();
                    }
                }
                $properties['update_main_images'] = $mainImages->pluck('id')->toArray();
            }

            $media = $model->getMedia('main_images')->pluck('file_name')->toArray();

            foreach ($request->input('main_images', []) as $file) {
                if (count($media) === 0 || !in_array($file, $media)) {
                    $model->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('main_images');
                }
            }

            //update logo
            $logo = $model->getMedia('logo');
            if (count($logo) > 0) {
                foreach ($logo as $l) {
                    if (!in_array($l->file_name, $request->input('logo', []))) {
                        $l->delete();
                    }
                }
                $properties['update_logo'] = $mainImages->pluck('id')->toArray();
            }

            $media = $model->getMedia('logo')->pluck('file_name')->toArray();

            foreach ($request->input('logo', []) as $file) {
                if (count($media) === 0 || !in_array($file, $media)) {
                    $model->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('logo');
                }
            }

            activity()
                ->performedOn($model)
                ->causedBy(auth()->user())
                ->withProperties($properties)
                ->setEvent('updated')
                ->log('updated');

            DB::commit();
            return $model;
        }
        catch (\Exception $exception){
            DB::rollBack();
            return  false;
        }
    }
}
