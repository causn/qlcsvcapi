<?php

namespace App\Repositories\Admin;

use App\Models\History;
use App\Repositories\BaseRepository;

class HistoryRepository extends BaseRepository
{
    protected $model = History::class;
}
