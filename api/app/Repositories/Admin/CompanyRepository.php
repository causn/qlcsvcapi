<?php

namespace App\Repositories\Admin;

use App\Models\Company;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CompanyRepository extends BaseRepository
{
    protected $model = Company::class;

    public function create(Request $request)
    {
        DB::beginTransaction();
        try {
            $attributes = $request->only((new $this->model)->getFillable() ?? []);

            $model = Company::create($attributes);

            $auth = auth()->user();
            if($auth->hasRole(['administrator'])){
                $universityId = $attributes['university'] ?? null;
            }else{
                $university = auth()->user()->university()->first();
                $universityId = $university->id ?? null;
            }

            if($universityId){
                $model->university()->detach();
                $model->university()->attach($universityId);
            }

            DB::commit();
            return $model;
        }catch (\Exception $exception){
            DB::rollBack();
            return false;
        }
    }

    public function update(Model $model, Request $request)
    {
        try{
            $attributes = $request->only((new $this->model)->getFillable() ?? []);
            $model->fill($attributes);
            $model->save();
            return $model;
        }catch(\Exception $exception){
            return false;
        }
    }
}
