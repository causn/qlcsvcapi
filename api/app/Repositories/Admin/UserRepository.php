<?php

namespace App\Repositories\Admin;

use App\Models\Role;
use App\Models\User;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserRepository extends BaseRepository
{
    protected $model = User::class;

    public function getListWithFilter(Request $request,$orders = [],$limit = null){
        $attributes = $request->only((new $this->model)->getFillable() ?? []);
        $attributes['level'] = ['>='=>auth()->user()->level];
        $query = $this->getQueryWithFilter($attributes,$orders,$limit);
        return $query->get();
    }

    public function getListWithFilterNotMe(Request $request,$orders = [],$limit = null){
        $attributes = $request->only((new $this->model)->getFillable() ?? []);
        $attributes['level'] = ['>='=>auth()->user()->level];
        $attributes['id'] = ['<>'=>auth()->user()->id];
        $query = $this->getQueryWithFilter($attributes,$orders,$limit);
        return $query->get();
    }

    public function getQueryWithFilter($attributes = [],$orders = [],$limit = null){
        $query = $this->model::query();
        $query = $this->setBaseCriteria($query);
        $query = $this->setBaseOrder($query);

        if(is_array($attributes) && count($attributes) > 0){
            foreach ($attributes as $key => $attribute){
                if(is_array($attribute)){
                    foreach ($attribute as $k => $at){
                        switch ($k){
                            case "in":
                                $query->whereIn($key, $at);
                                break;
                            case "not_in":
                                $query->whereNotIn($key, $at);
                                break;
                            default:
                                $query->where($key, $k, $at);
                                break;
                        }
                    }
                }else {
                    $query->where($key,$attribute);
                }
            }
        }

        if(is_array($orders) && count($orders) > 0){
            foreach ($orders as $key => $order){
                $query->orderBy($key,$order);
            }
        }

        if($limit){
            if($limit != 1){
                $query->limit($limit);
            }
        }

        return $query;
    }

    public function create(Request $request){
        DB::beginTransaction();
        try {
            $attributes = $request->only((new $this->model)->getFillable() ?? []);
            $attributes['password'] = Hash::make($attributes['password']);
            $attributes['is_admin'] = User::ACTIVE_STATUS;
            $user = $this->model::create($attributes);

            if($request->has('role')){
                $role = Role::find($request->input('role'));
                $user->assignRole($role);
            }

            $auth = auth()->user();
            if($auth->hasRole(['administrator'])){
                $universityId = $request->input('university') ?? null;
            }else{
                $university = auth()->user()->university()->first();
                $universityId = $university->id ?? null;
            }

            if($universityId){
                $user->university()->detach();
                $user->university()->attach($universityId);
            }

            if($request->filled('avatar')) {
                foreach ($request->input('avatar', []) as $file) {
                    $user->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('avatar');
                }
            }

            DB::commit();
            return $user;
        }catch (\Exception $exception){
            DB::rollBack();
            return false;
        }
    }

    /**
     * @param User $user
     * @param Request $request
     * @return false|Model
     */
    public function update($user, Request $request){
        DB::beginTransaction();
        try {
            $attributes = $request->only((new $this->model)->getFillable() ?? []);

            if (!empty($attributes['password'])) {
                $attributes['password'] = Hash::make($attributes['password']);
            } else {
                $attributes = Arr::except($attributes, array('password'));
            }

            $user->update($attributes);

            if($request->has('role')){
                DB::table('model_has_roles')->where('model_id', $user->id)->delete();
                $role = Role::find($request->input('role'));
                $user->assignRole($role);
            }

            $auth = auth()->user();
            if($auth->hasRole(['administrator'])){
                $universityId = $request->input('university') ?? null;
            }else{
                $university = auth()->user()->university()->first();
                $universityId = $university->id ?? null;
            }

            if($universityId){
                $user->university()->detach();
                $user->university()->attach($universityId);
            }

            if($request->filled('avatar')) {
                $avatar = $user->getMedia('avatar');
                if (count($avatar) > 0) {
                    foreach ($avatar as $media) {
                        if (!in_array($media->file_name, $request->input('avatar', []))) {
                            $media->delete();
                        }
                    }
                }

                $media = $user->getMedia('avatar')->pluck('file_name')->toArray();

                foreach ($request->input('avatar', []) as $file) {
                    if (count($media) === 0 || !in_array($file, $media)) {
                        $user->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('avatar');
                    }
                }
            }

            DB::commit();
            return $user;
        }
        catch (\Exception $exception){
            DB::rollBack();
            return  false;
        }
    }

    public function register($attributes = []){
        DB::beginTransaction();
        try {
            $attributes['password'] = Hash::make($attributes['password']);
            $attributes['status'] = User::DRAFT_STATUS;
            $attributes['is_admin'] = User::DRAFT_STATUS;
            /** @var User $user */
            $user = $this->model::create($attributes);

            $user->sendEmailVerificationNotification();

            DB::commit();
            return $user;
        }catch (\Exception $exception){
            DB::rollBack();
            return false;
        }
    }
}
