<?php

namespace App\Repositories\Admin;

use App\Models\Activity;
use App\Models\User;
use App\Repositories\BaseRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AccessSystemRepository extends BaseRepository
{
    protected $model = Activity::class;
    protected $defaultOrder = ['id'=>'desc'];

    public function getListWithFilter(Request $request,$orders = [],$limit = null){
        $attributes = $request->only((new $this->model)->getFillable() ?? []);

        $query = Activity::query();
        $query->select('*',DB::raw('GROUP_CONCAT(created_at) as log_time'),DB::raw('GROUP_CONCAT(event) as event_time'));
        $query->whereIn('event',['logged','logged-out']);

        if($request->has('users')){
            $model = User::class;
            $subject = new \ReflectionClass($model);
            $query->where('subject_type', $subject->getName());
            $query->whereIn('subject_id', $request->get('users'));
        }

        $query = $this->setCustomCondition($query,$attributes);

        $query->groupBy('batch_uuid');

        return $query->get();
    }
}
