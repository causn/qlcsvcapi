<?php

namespace App\Repositories\Admin;

use App\Models\AssetType;
use App\Models\AssetTypeItemAttribute;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;

class AssetTypeRepository extends BaseRepository
{
    protected $model = AssetType::class;

    public function update(Model $model,Request $request)
    {
        DB::beginTransaction();
        try{
            $attributes = $request->only((new $this->model)->getFillable() ?? []);
            $assetType = $this->model->fill($attributes);
            $assetType->save();

            $attributesRequest = $this->formatInputAttributes($request->input('attributes'));

            if(count($attributesRequest) > 0) {
                foreach ($attributesRequest as $key =>  $value) {
                    $assetTypeAttribute = AssetTypeItemAttribute::query()
                        ->where('asset_type_id',$assetType->id)
                        ->where('asset_attribute_id',$key)
                        ->first();
                    if($assetTypeAttribute){
                        $assetTypeAttribute->value = $value;
                        $assetTypeAttribute->save();
                    }else{
                        $assetType->assetAttribute()->attach($key,['value' => $value]);
                    }
                }
            }

            //add media
            if($request->file('main')) {
                $this->updateMedia($request->file('main'),$assetType,'main_asset_type');
            }

            DB::commit();
            return $model;
        }catch(\Exception $exception){
            DB::rollback();
            return false;
        }
    }

    private function updateMedia(UploadedFile $file, $assetType,$name = 'asset_type'){
        $assetType->clearMediaGroup($name);
        $ext = explode('/', $file->getMimeType())[1];
        $name = $name.'_'.time();
        $assetTypeMedia = MediaUploader::fromFile($file)
            ->useFileName($name.'.'.$ext)
            ->useName($name)
            ->upload();
        $assetType->attachMedia($assetTypeMedia,$name);
    }

    public function create(Request $request){
        DB::beginTransaction();
        try {
            $attributes = $request->only((new $this->model)->getFillable() ?? []);
            $assetType = $this->model::create($attributes);

            $attributesRequest =  $this->formatInputAttributes($request->input('attributes'));

            if(count($attributesRequest) > 0) {
                foreach ($attributesRequest as $key =>  $value) {
                    $assetTypeAttribute = AssetTypeItemAttribute::query()
                        ->where('asset_type_id',$assetType->id)
                        ->where('asset_attribute_id',$key)
                        ->first();
                    if($assetTypeAttribute){
                        $assetTypeAttribute->value = $value;
                        $assetTypeAttribute->save();
                    }else{
                        $assetType->assetAttribute()->attach($key,['value' => $value]);
                    }
                }
            }

            if($request->file('main')) {
                $this->updateMedia($request->file('main'),$assetType,'main_asset_type');
            }

            DB::commit();
            return $assetType;
        }catch (\Exception $exception){
            DB::rollBack();
            return false;
        }
    }

    private function formatInputAttributes($inputs) {
        $result = [];

        foreach($inputs as $key => $input){
            if(is_array($input)){
                $values = [];
                foreach($input as $value){
                    if(!empty($value['key']) || !empty($value['value'])){
                        $values[] = $value;
                    }
                }
                $result[$key] = json_encode($values,JSON_UNESCAPED_UNICODE);
            }else{
                $result[$key] = $input;
            }
        }

        return $result;
    }
}
