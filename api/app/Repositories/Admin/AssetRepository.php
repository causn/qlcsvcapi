<?php

namespace App\Repositories\Admin;

use App\Models\Asset;
use App\Models\AssetType;
use App\Models\Rent;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AssetRepository extends BaseRepository
{
    protected $model = Asset::class;

    public function create(Request $request){
        DB::beginTransaction();
        try {
            $attributes = $request->only((new $this->model)->getFillable() ?? []);

            $asset = Asset::create($attributes);
            if($request->has('asset_type')){
                $assetType = AssetType::query()->where('id',$request->input('asset_type'))->first();
                $asset->assetType()->detach();
                $asset->assetType()->attach($assetType->id);
            }

            $auth = auth()->user();
            if($auth->hasRole(['administrator'])){
                $universityId = $attributes['university'] ?? null;
            }else{
                $university = auth()->user()->university()->first();
                $universityId = $university->id ?? null;
            }

            if($universityId){
                $asset->university()->detach();
                $asset->university()->attach($universityId);
            }

            foreach ($request->input('main_images', []) as $file) {
                $asset->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('main_images');
            }

            DB::commit();
            return $asset;
        }catch (\Exception $exception){
            DB::rollBack();
            return false;
        }
    }

    public function show($id){
        $request = new Request();
        $request->merge(['id'=>$id]);
        $asset = $this->getActive($request,[],1);
        return $asset;
    }

    public function update($asset, Request $request){
        DB::beginTransaction();
        try {
            $attributes = $request->only((new $this->model)->getFillable() ?? []);
            $properties = $attributes;
            foreach ($attributes as $key => $attribute){
                $asset->$key = $attribute;
            }

            $asset->save();

            if($request->has('asset_type')){
                $assetType = AssetType::query()->where('id',$request->get('asset_type'))->first();
                $asset->assetType()->detach();
                $asset->assetType()->attach($assetType->id);
            }

            $mainImages = $asset->getMedia('main_images');
            if (count($mainImages) > 0) {
                foreach ($mainImages as $media) {
                    if (!in_array($media->file_name, $request->input('main_images', []))) {
                        $media->delete();
                    }
                }
                $properties['update_main_images'] = $mainImages->pluck('id')->toArray();
            }

            $media = $asset->getMedia('main_images')->pluck('file_name')->toArray();

            foreach ($request->input('main_images', []) as $file) {
                if (count($media) === 0 || !in_array($file, $media)) {
                    $asset->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('main_images');
                }
            }

            activity()
                ->performedOn($asset)
                ->causedBy(auth()->user())
                ->withProperties($properties)
                ->setEvent('updated')
                ->log('updated');

            DB::commit();
            return $asset;
        }
        catch (\Exception $exception){
            DB::rollBack();
            return  false;
        }
    }

    private function setLiquidationActiveQuery(Builder $builder){
        $builder->whereDoesntHave('liquidations', function ($query) {
            $query->where('status','!=',Rent::DRAFT_STATUS)
                  ->where('end_time', '<', Carbon::now())
                  ->orWhere('start_time', '>', Carbon::now());
        });

        return $builder;
    }

    private function setRentActiveQuery(Builder $builder){
        $builder->whereDoesntHave('rent', function ($query) {
            $query->where('status','!=',Rent::DRAFT_STATUS)
                  ->where('end_time', '<', Carbon::now())
                  ->orWhere('start_time', '>', Carbon::now());
        });
        return $builder;
    }

    public function getActive(Request $request,$orders = [],$limit = null,$type = null){
        $attributes = $request->only((new $this->model)->getFillable() ?? []);
        $query = $this->model::query();
        $query->status = Asset::APPROVED_STATUS;
        if(is_null($type)) {
            $query = $this->setRentActiveQuery($query);
            $query = $this->setLiquidationActiveQuery($query);
        }else{
            if($type == 'rent'){
                $query = $this->setRentActiveQuery($query);
            }
            if($type == 'liquidation'){
                $query = $this->setLiquidationActiveQuery($query);
            }
        }

        $query = $this->setBaseCriteria($query);
        $query = $this->setBaseOrder($query);

        $query = $this->setCustomCondition($query,$attributes);

        $query = $this->setCustomOrder($query,$orders);

        return $this->getResult($query,$limit);
    }


    public function getAssetAreInUse(Request $request){
        $startDate = $request->filled('start_time') ? Carbon::createFromFormat('d/m/Y',$request->query('start_time'))->format('Y-m-d') : null;
        $endDate = $request->filled('end_time') ? Carbon::createFromFormat('d/m/Y',$request->query('end_time'))->format('Y-m-d') : null;
        $id = $request->filled('id') ? (int)$request->get('id') : null;
        $code = $request->filled('code') ? (int)$request->get('code') : null;

        $query = DB::table('assets')
            ->select(DB::raw('COALESCE(SUM(`asset_stock`.`number`),0) AS `used`, `assets`.`number` as `total` '))
            ->leftJoin('asset_stock', function (JoinClause $join) use ($startDate,$endDate) {
                $join->on('assets.id', '=', 'asset_stock.asset_id');
                if($startDate) {
                    $join->where('asset_stock.start_date', '>=', $startDate);
                }
                if($endDate) {
                    $join->where('asset_stock.end_date', '<=', $endDate);
                }
            })
            ->where('assets.status',Asset::APPROVED_STATUS);

        if($id){
            $query->where('assets.id',$id);
        }

        if($code){
            $query->where('assets.code',$code);
        }

        $result = $query->first();

        if(is_null($result->total) || (int)$result->total == 0 || (int)$result->total == (int)$result->used){
            return  false;
        }

        $assetQuery = Asset::query();
        if($id){
            $assetQuery->where('id',$id);
        }

        if($code){
            $assetQuery->where('code',$code);
        }

        $asset = $assetQuery->first();
        $asset->used = $result->used;
        $asset->total = $result->total;

        return  $asset;
    }
}
