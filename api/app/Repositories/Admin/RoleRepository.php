<?php

namespace App\Repositories\Admin;

use App\Models\Role;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RoleRepository extends BaseRepository
{
    protected $model = Role::class;

    public function setBaseCriteria(Builder $query){
        /** @var User $user */
        $user = auth()->user();

        if($user->hasRole(config('permission.role_is_supper_admin_name'))){
            return $query;
        }

        return  $query;
    }

    public function getListWithFilter(Request $request,$orders = [],$limit = null){
        $attributes = $request->only((new $this->model)->getFillable() ?? []);

        if(!empty($attributes['name'])){
            $attributes['name'][] = $attributes['name'];
            $attributes['name'][] = [
                '!=' => 'administrator'
            ];
        }else{
            $attributes['name'] = [
                '!=' => 'administrator'
            ];
        }

        $query = $this->getQueryWithFilter($attributes,$orders,$limit);

        return $query->get();
    }

    public function getListWithFilterLess(Request $request,$orders = [],$limit = null){
        $attributes = $request->only((new $this->model)->getFillable() ?? []);

        $roleConfig = null;
        if(auth()->user() && auth()->user()->roles->pluck('name')->first() != 'administrator'){
            switch (auth()->user()->roles->pluck('name')->first()){
                case 'admin':
                    $roleConfig = ['admin','director','manager','staff'];
                    break;
                case 'director':
                    $roleConfig = ['director','manager','staff'];
                    break;
                case 'manager':
                    $roleConfig = ['manager','staff'];
                    break;
            }

            if(!empty($attributes['name'])){
                $attributes['name'][] = $attributes['name'];
                $attributes['name'][] = ['in'=>$roleConfig];
            }else{
                $attributes['name'] = ['in'=>$roleConfig];
            }
        }

        $query = $this->getQueryWithFilter($attributes,$orders,$limit);

        return $query->get();
    }

    public function create(Request $request){
        DB::beginTransaction();
        try {
            $attributes = $request->only((new $this->model)->getFillable() ?? []);
            $role = $this->model::create($attributes);

            $role->syncPermissions($request->input('permissions'));

            DB::commit();
            return $role;
        }catch (\Exception $exception){
            DB::rollBack();
            return false;
        }
    }

    public function update(Model $model,Request $request)
    {
        DB::beginTransaction();
        try{
            $attributes = $request->only((new $this->model)->getFillable() ?? []);

            foreach ($attributes as $key => $attribute){
                $model->$key = $attribute;
            }

            $model->save();

            $model->syncPermissions($request->input('permissions'));

            DB::commit();
            return $model;
        }catch(\Exception $exception){
            DB::rollback();
            return false;
        }
    }
}
