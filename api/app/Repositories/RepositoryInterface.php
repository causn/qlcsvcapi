<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

interface RepositoryInterface
{
    /**
     * Get one
     * @param $id
     * @return mixed
     */
    public function find($id);

    /**
     * Create
     * @param array $attributes
     * @return mixed
     */
    public function create(Request $request);

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function update(Model $model, Request $request);

    /**
     * Delete
     * @param $id
     * @return mixed
     */
    public function destroy(Model $model);
}
