<?php

namespace App\Repositories;

use App\Models\University;
use Illuminate\Http\Request;

class UniversityRepository
{
    public function getList(Request $request){
        $list =  University::query()->get();
        foreach ($list as &$item){
            $item->logo = $item->getFirstMedia('logo')->toArray();
            $item->main_images = $item->getMedia('main_images')->toArray();
        }
        return $list;
    }

    public function findId($id){
        return University::query()->where('id',$id)->first();
    }
}
