<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AccessSystemController;
use App\Http\Controllers\Admin\AssetAttributeController;
use App\Http\Controllers\Admin\AssetTypeController;
use App\Http\Controllers\Admin\CompanyController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\LiquidationController;
use App\Http\Controllers\Admin\RentController;
use App\Http\Controllers\Admin\SystemConfigController;
use App\Http\Controllers\Admin\SystemDatabaseController;
use App\Http\Controllers\Admin\UniversityController as AdminUniversityController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\FrontEnd\AssetController;
use App\Http\Controllers\Admin\AssetController as AdminAssetController;
use App\Http\Controllers\FrontEnd\CartController;
use App\Http\Controllers\FrontEnd\MediaController;
use App\Http\Controllers\FrontEnd\ProfileController;
use App\Http\Controllers\FrontEnd\UniversityController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

//university
Route::get('/university', [UniversityController::class, 'list'])->name('api.university.list');
Route::get('/university/{id}', [UniversityController::class, 'detail'])->name('api.university.detail');
//assets
Route::get('/assets', [AssetController::class, 'list'])->name('api.asset.list');
Route::get('/asset/{asset}', [AssetController::class, 'detail'])->name('api.asset.detail');
Route::post('/asset/check-active', [AssetController::class, 'checkActive'])->name('api.asset.check.active');
//auth
Route::post('/login', [AuthController::class, 'login'])->name('api.login');

//Route::get('/profile', [ProfileController::class, 'index'])
//    ->middleware(['auth:sanctum','ability:user-list, profile'])
//    ->name('api.profile');
//Route::put('/profile', [ProfileController::class, 'update'])
//    ->middleware(['auth:sanctum','ability:user-edit,profile'])
//    ->name('api.update.profile');
//Route::post('/upload/media', [ProfileController::class, 'index'])
//    ->middleware(['auth:sanctum','ability:user-list'])
//    ->name('api.profile');

Route::middleware('auth:sanctum')->group(function () {
    //profile
    Route::get('/profile', [ProfileController::class, 'index'])->name('api.profile');
    Route::put('/profile', [ProfileController::class, 'update'])->name('api.update.profile');
    Route::get('/profile/rent-status', [ProfileController::class, 'rentStatus'])->name('api.profile.rent.status');
    //media
    Route::post('upload/media', [MediaController::class, 'storeMedia'])->name('api.upload.media');
    //cart
    Route::get('/cart', [CartController::class, 'list'])->name('api.cart');
    Route::put('/cart/update', [CartController::class, 'update'])->name('api.cart.update');
    //BACKEND
    Route::group(['prefix' => 'admin', 'as' => 'api.admin.'], function () {
        Route::get('/', [HomeController::class, 'index'])->name('api.admin.home');

        Route::get('users',[UserController::class, 'index'])->name('api.admin.users.index');
        Route::put('users',[UserController::class, 'store'])->name('api.admin.users.store');
        Route::get('users/{user}',[UserController::class, 'detail'])->name('api.admin.users.detail');
        Route::post('users/{user}',[UserController::class, 'update'])->name('api.admin.users.update');
        Route::delete('users/{user}',[UserController::class, 'destroy'])->name('api.admin.users.destroy');

        Route::get('company',[CompanyController::class, 'index'])->name('api.admin.company.index');
        Route::put('company',[CompanyController::class, 'store'])->name('api.admin.company.store');
        Route::get('company/{company}',[CompanyController::class, 'detail'])->name('api.admin.company.detail');
        Route::post('company/{company}',[CompanyController::class, 'update'])->name('api.admin.company.update');
        Route::delete('company/{company}',[CompanyController::class, 'destroy'])->name('api.admin.company.destroy');

        Route::get('university',[AdminUniversityController::class, 'index'])->name('api.admin.university.index');
        Route::put('university',[AdminUniversityController::class, 'store'])->name('api.admin.university.store');
        Route::get('university/{university}',[AdminUniversityController::class, 'detail'])->name('api.admin.university.detail');
        Route::post('university/{university}',[AdminUniversityController::class, 'update'])->name('api.admin.university.update');
        Route::delete('university/{university}',[AdminUniversityController::class, 'destroy'])->name('api.admin.university.destroy');

        Route::get('liquidation',[LiquidationController::class, 'index'])->name('api.admin.liquidation.index');
        Route::put('liquidation',[LiquidationController::class, 'store'])->name('api.admin.liquidation.store');
        Route::get('liquidation/{liquidation}',[LiquidationController::class, 'detail'])->name('api.admin.liquidation.detail');
        Route::post('liquidation/{liquidation}',[LiquidationController::class, 'update'])->name('api.admin.liquidation.update');
        Route::delete('liquidation/{liquidation}',[LiquidationController::class, 'destroy'])->name('api.admin.liquidation.destroy');

        Route::get('rent',[RentController::class, 'index'])->name('api.admin.rent.index');
        Route::put('rent',[RentController::class, 'store'])->name('api.admin.rent.store');
        Route::get('rent/{rent}',[RentController::class, 'detail'])->name('api.admin.rent.detail');
        Route::post('rent/{rent}',[RentController::class, 'update'])->name('api.admin.rent.update');
        Route::delete('rent/{rent}',[RentController::class, 'destroy'])->name('api.admin.rent.destroy');

        Route::get('asset',[AdminAssetController::class, 'index'])->name('api.admin.asset.index');
        Route::put('asset',[AdminAssetController::class, 'store'])->name('api.admin.asset.store');
        Route::get('asset/filter', [AdminAssetController::class, 'filter'])->name('api.admin.asset.filter');
        Route::get('asset/{asset}',[AdminAssetController::class, 'detail'])->name('api.admin.asset.detail');
        Route::post('asset/{asset}',[AdminAssetController::class, 'update'])->name('api.admin.asset.update');
        Route::delete('asset/{asset}',[AdminAssetController::class, 'destroy'])->name('api.admin.asset.destroy');

        Route::resource('asset-type', AssetTypeController::class)
            ->parameters([
                'asset_type' => 'assetType'
            ]);
        Route::resource('asset-attribute', AssetAttributeController::class)
            ->parameters([
                'asset_attribute' => 'assetAttribute'
            ]);
        Route::get('access-system', [AccessSystemController::class, 'index'])->name('api.admin.access-system');
        Route::get('system-database', [SystemDatabaseController::class, 'index'])->name('api.admin.access_system_database');
        Route::get('system-database/backup', [SystemDatabaseController::class, 'backup'])->name('api.admin.access-system-backup');
        Route::get('system-database/restore/{name}/{fileName}', [SystemDatabaseController::class, 'restore'])->name('api.admin.access-system-restore');
        Route::get('system-database/delete/{name}/{fileName}', [SystemDatabaseController::class, 'delete'])->name('api.admin.access-system-delete');
        Route::get('system-config', [SystemConfigController::class, 'edit'])->name('api.admin.system-config.edit');
        Route::post('system-config', [SystemConfigController::class, 'update'])->name('api.admin.system-config.update');
    });
});

