<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRentUniversitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rent_universities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBiginteger('rent_id')->unsigned();
            $table->unsignedBiginteger('university_id')->unsigned();

            $table->foreign('rent_id')->references('id')
                ->on('rent')->onDelete('cascade');
            $table->foreign('university_id')->references('id')
                ->on('universities')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rent_universities');
    }
}
