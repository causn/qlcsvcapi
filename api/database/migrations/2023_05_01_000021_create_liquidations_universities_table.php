<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLiquidationsUniversitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('liquidations_universities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBiginteger('liquidations_id')->unsigned();
            $table->unsignedBiginteger('university_id')->unsigned();

            $table->foreign('liquidations_id')->references('id')
                ->on('rent')->onDelete('cascade');
            $table->foreign('university_id')->references('id')
                ->on('universities')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('liquidations_universities');
    }
}
