<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetsTypeItemAttributeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets_type_item_attribute', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('asset_type_id')->nullable()->unsigned();
            $table->unsignedBigInteger('asset_attribute_id')->nullable()->unsigned();
            $table->longText('value')->nullable();
            $table->foreign('asset_type_id')->references('id')->on('assets_type')
                ->onDelete('cascade');
            $table->foreign('asset_attribute_id')->references('id')->on('assets_attributes')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets_type_item_attribute');
    }
}
