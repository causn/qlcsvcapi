<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLiquidationsCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('liquidations_companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBiginteger('liquidations_id')->unsigned();
            $table->unsignedBiginteger('company_id')->unsigned();

            $table->foreign('liquidations_id')->references('id')
                ->on('rent')->onDelete('cascade');
            $table->foreign('company_id')->references('id')
                ->on('companies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('liquidations_companies');
    }
}
