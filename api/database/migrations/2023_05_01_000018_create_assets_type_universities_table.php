<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetsTypeUniversitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets_type_universities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBiginteger('asset_type_id')->unsigned();
            $table->unsignedBiginteger('university_id')->unsigned();

            $table->foreign('asset_type_id')->references('id')
                ->on('assets_type')->onDelete('cascade');
            $table->foreign('university_id')->references('id')
                ->on('universities')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets_type_universities');
    }
}
