<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesUniversitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies_universities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBiginteger('company_id')->unsigned();
            $table->unsignedBiginteger('university_id')->unsigned();

            $table->foreign('company_id')->references('id')
                ->on('companies')->onDelete('cascade');
            $table->foreign('university_id')->references('id')
                ->on('universities')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies_universities');
    }
}
