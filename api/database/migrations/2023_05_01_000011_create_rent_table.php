<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rent', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('asset_code')->nullable();
            $table->text('status')->nullable();
            $table->dateTime('start_time')->nullable();
            $table->dateTime('end_time')->nullable();
            $table->integer('qty')->nullable();
            $table->text('unit')->nullable();
            $table->text('price')->nullable();
            $table->unsignedBiginteger('create_by')->unsigned()->nullable();
            $table->unsignedBiginteger('update_by')->unsigned()->nullable();
            $table->timestamps();
        });

        Schema::table('rent', function (Blueprint $table) {
            $table->foreign('asset_code')->references('code')
                ->on('assets')->onDelete('SET NULL');
            $table->foreign('create_by')->references('id')
                ->on('users')->onDelete('SET NULL');
            $table->foreign('update_by')->references('id')
                ->on('users')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rent');
    }
}
