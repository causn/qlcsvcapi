<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetItemAssetTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_item_asset_type', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('asset_id')->nullable()->unsigned();
            $table->unsignedBigInteger('asset_type_id')->nullable()->unsigned();
            $table->foreign('asset_id')->references('id')->on('assets')
                ->onDelete('cascade');
            $table->foreign('asset_type_id')->references('id')->on('assets_type')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_item_asset_type');
    }
}
