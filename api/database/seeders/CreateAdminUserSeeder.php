<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\University;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Auth;

class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleAdministrator = Role::query()->where('name','administrator')->first();

        $administrator = new User();
        $administrator->name = 'administrator';
        $administrator->email = 'administrator@gmail.com';
        $administrator->password = bcrypt('12345679');
        $administrator->level = 0;
        $administrator->is_admin = User::ACTIVE_STATUS;
        $administrator->email_verified_at = Carbon::now();
        $administrator->save();

        Auth::login($administrator);
        $administrator->assignRole([$roleAdministrator->id]);

        $roleAdmin = Role::query()->where('name','admin')->where('guard_name','admin')->first();
        $roleDirector = Role::query()->where('name','director')->where('guard_name','admin')->first();
        $roleManager = Role::query()->where('name','manager')->where('guard_name','admin')->first();
        $roleStaff = Role::query()->where('name','staff')->where('guard_name','admin')->first();

        $universities = ['001'=>'dhtn','002'=>'dhnn','003'=>'dhkh','004'=>'dhcntt'];

        foreach ($universities as $code => $alias){
            $university = University::query()->where('code', $code)->first();

            $admin = User::create([
                'name' => 'Quản trị viên',
                'email' => 'admin.'.$alias.'@gmail.com',
                'password' => bcrypt('12345679'),
                'level' => 1,
                'is_admin' => User::ACTIVE_STATUS,
                'email_verified_at' => Carbon::now()
            ]);
            $admin->assignRole([$roleAdmin->id]);
            $admin->university()->detach();
            $admin->university()->attach($university->id);

            $director = User::create([
                'name' => 'Hiệu trưởng',
                'email' => 'director.'.$alias.'@gmail.com',
                'password' => bcrypt('12345679'),
                'level' => 2,
                'is_admin' => User::ACTIVE_STATUS,
                'email_verified_at' => Carbon::now()
            ]);
            $director->assignRole([$roleDirector->id]);
            $director->university()->detach();
            $director->university()->attach($university->id);

            $manager = User::create([
                'name' => 'Trưởng khoa',
                'email' => 'manager.'.$alias.'@gmail.com',
                'password' => bcrypt('12345679'),
                'level' => 3,
                'is_admin' => User::ACTIVE_STATUS,
                'email_verified_at' => Carbon::now()
            ]);
            $manager->assignRole([$roleManager->id]);
            $manager->university()->detach();
            $manager->university()->attach($university->id);

            $staff = User::create([
                'name' => 'Giảng viên',
                'email' => 'staff.'.$alias.'@gmail.com',
                'password' => bcrypt('12345679'),
                'level' => 4,
                'is_admin' => User::ACTIVE_STATUS,
                'email_verified_at' => Carbon::now()
            ]);
            $staff->assignRole([$roleStaff->id]);
            $staff->university()->detach();
            $staff->university()->attach($university->id);
        }
    }
}
