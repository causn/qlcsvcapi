<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\University;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $codeArr = ['001','002','003','004'];
        $attributes  = [
            [
                'name' => 'Công Ty TNHH Cửa Sổ Việt Châu Á Asean window Viet Asian Window Asean window Co.,Ltd',
                'address' => '191 Lê Trọng Tấn, P. Sơn Kỳ, Q. Tân Phú, Tp. Hồ Chí Minh',
                'phone' => '02862686132',
                'created_at' => Carbon::now(),
            ],
            [
                'name' => 'Việt Nam Window',
                'address' => 'Toà nhà Elipse Tower, 110 Trần Phú, Hà Đông, Hà Nội.',
                'phone' => '02462958958',
                'created_at' => Carbon::now()
            ],
            [
                'name' => 'Nội thất Hòa Phát',
                'address' => 'B-TT01-46, Khu nhà ở Ngân Hà Vạn Phúc, Phường Vạn Phúc, Quận Hà Đông, Thành phố Hà Nội, Việt Nam',
                'phone' => '02433676688',
                'created_at' => Carbon::now()
            ],
            [
                'name' => 'Công Ty TNHH Nội Thất Minh Đức',
                'address' => 'D04 -L26, An Phú, Dương Nội, Hà Đông, Hà Nội',
                'phone' => '0908314939',
                'created_at' => Carbon::now()
            ]
        ];
        foreach ($codeArr as $code){
            $university = University::query()->where('code',$code)->first();
            if($university){
                $index = 1;
                foreach ($attributes as $attribute){
                    $attribute['code'] = sprintf("%03d", $index);
                    $company = Company::create($attribute);
                    $company->university()->detach();
                    $company->university()->attach($university->id);
                    $index++;
                }
            }
        }
    }
}
