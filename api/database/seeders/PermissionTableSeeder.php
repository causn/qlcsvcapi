<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'user-list',
            'user-create',
            'user-edit',
            'user-delete',
            'profile',
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'permission-list',
            'permission-create',
            'permission-edit',
            'permission-delete',
            'asset-list',
            'asset-create',
            'asset-edit',
            'asset-delete',
            'asset-type-list',
            'asset-type-create',
            'asset-type-edit',
            'asset-type-delete',
            'asset-type-show',
            'asset-attribute-list',
            'asset-attribute-create',
            'asset-attribute-edit',
            'asset-attribute-delete',
            'asset-attribute-show',
            'company-list',
            'company-create',
            'company-edit',
            'company-delete',
            'liquidation-list',
            'liquidation-create',
            'liquidation-edit',
            'liquidation-delete',
            'rent-list',
            'rent-create',
            'rent-edit',
            'rent-delete',
            'university-list',
            'university-create',
            'university-edit',
            'university-delete',
            'profile'
        ];

        foreach ($permissions as $permission) {
            $item = Permission::query()->where('name',$permission)->where('guard_name','admin')->first();
            if(!$item) {
                Permission::create(['name' => $permission,'guard_name'=>'admin']);
            }
        }

        $roles = [
            'admin' => [
                'user-list',
                'user-create',
                'user-edit',
                'user-delete',
                'profile',
                'role-list',
                'role-create',
                'role-edit',
                'role-delete',
                'permission-list',
                'permission-create',
                'permission-edit',
                'permission-delete',
                'asset-list',
                'asset-create',
                'asset-edit',
                'asset-delete',
                'asset-type-list',
                'asset-type-create',
                'asset-type-edit',
                'asset-type-delete',
                'asset-type-show',
                'asset-attribute-list',
                'asset-attribute-create',
                'asset-attribute-edit',
                'asset-attribute-delete',
                'asset-attribute-show',
                'company-list',
                'company-create',
                'company-edit',
                'company-delete',
                'liquidation-list',
                'liquidation-create',
                'liquidation-edit',
                'liquidation-delete',
                'rent-list',
                'rent-create',
                'rent-edit',
                'rent-delete',
                'profile'
            ],
            'director' => [
                'asset-list',
                'asset-create',
                'asset-edit',
                'asset-delete',
                'asset-type-list',
                'asset-type-create',
                'asset-type-edit',
                'asset-type-delete',
                'asset-type-show',
                'asset-attribute-list',
                'asset-attribute-create',
                'asset-attribute-edit',
                'asset-attribute-delete',
                'asset-attribute-show',
                'liquidation-list',
                'liquidation-create',
                'liquidation-edit',
                'liquidation-delete',
                'rent-list',
                'rent-create',
                'rent-edit',
                'rent-delete',
                'user-list',
                'user-edit',
                'profile'
            ],
            'manager' => [
                'asset-list',
                'asset-create',
                'asset-edit',
                'asset-delete',
                'asset-type-list',
                'asset-type-create',
                'asset-type-edit',
                'asset-type-delete',
                'asset-type-show',
                'asset-attribute-list',
                'asset-attribute-create',
                'asset-attribute-edit',
                'asset-attribute-delete',
                'asset-attribute-show',
                'liquidation-list',
                'liquidation-create',
                'liquidation-edit',
                'liquidation-delete',
                'rent-list',
                'rent-create',
                'rent-edit',
                'rent-delete',
                'profile',
                'user-list'
            ],
            'staff' => [
                'asset-list',
                'asset-create',
                'asset-edit',
                'asset-delete',
                'liquidation-list',
                'liquidation-create',
                'liquidation-edit',
                'liquidation-delete',
                'rent-list',
                'rent-create',
                'rent-edit',
                'rent-delete',
                'profile',
                'asset-type-show',
                'asset-attribute-show',
            ]
        ];

        try {

            foreach ($roles as $key => $items) {
                $role = Role::query()
                    ->where('name', $key)
                    ->where('guard_name', 'admin')
                    ->first();

                if (!$role) {
                    $role = Role::create(['name' => $key, 'guard_name' => 'admin']);
                }

                $permissions = Permission::query()
                    ->whereIn('name', $items)
                    ->where('guard_name','admin')
                    ->get()
                    ->pluck('id', 'id');

                $role->syncPermissions($permissions);
            }

            $role = Role::query()->where('name', 'administrator')->where('guard_name', 'admin')->first();
            if (!$role) {
                $role = Role::create(['name' => 'administrator', 'guard_name' => 'admin']);
            }

            $permissions = Permission::query()->get()->pluck('id', 'id');
            $role->syncPermissions($permissions);
        }
        catch (\Exception $exception){
          Log::error("permission seeder error:".$exception->getMessage());
        }
    }
}
