<?php

namespace Database\Seeders;

use App\Models\Asset;
use App\Models\AssetAttribute;
use App\Models\AssetType;
use App\Models\University;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Auth;

class AssetAttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $administrator = User::query()->where('email','administrator@gmail.com')->first();
        Auth::login($administrator);

        $universities = ['001','002','003','004'];

        $index = $indexType = $indexAsset = 1;
        foreach ($universities as $university) {
            $u = University::query()->where('code', $university)->first();

            $color = AssetAttribute::create([
                'name' => 'Màu sắc',
                'code' =>  sprintf("%03d", $index),
                'unit' => 'hex',
                'type' => 'select',
                'value' => '{"Đỏ":"#22222","Xanh":"#11111","Xám":"#33333"}',
                'created_at' => Carbon::now()
            ]);
            $color->setRelation('createBy', $administrator);
            $color->university()->detach();
            $color->university()->attach($u->id);
            $index++;

            $material = AssetAttribute::create([
                'name' => 'Chất liệu',
                'code' => sprintf("%03d", $index),
                'type' => 'select-value',
                'value' => '["Gỗ","Nhựa","Inox"]',
                'created_at' => Carbon::now()
            ]);
            $material->setRelation('createBy',$administrator);
            $material->university()->detach();
            $material->university()->attach($u->id);
            $index++;

            $length = AssetAttribute::create([
                'name' => 'Chiều dài',
                'code' => sprintf("%03d", $index),
                'unit' => 'cm',
                'type'=> 'text',
                'created_at' => Carbon::now()
            ]);
            $length->setRelation('createBy',$administrator);
            $length->university()->detach();
            $length->university()->attach($u->id);
            $index++;

            $height = AssetAttribute::create([
                'name' => 'Chiều cao',
                'code' => sprintf("%03d", $index),
                'unit' => 'cm',
                'type'=> 'text',
                'created_at' => Carbon::now()
            ]);
            $height->setRelation('createBy',$administrator);
            $height->university()->detach();
            $height->university()->attach($u->id);
            $index++;

            $weight = AssetAttribute::create([
                'name' => 'Chiều rộng',
                'code' => sprintf("%03d", $index),
                'unit' => 'cm',
                'type'=> 'text',
                'created_at' => Carbon::now()
            ]);
            $weight->setRelation('createBy',$administrator);
            $weight->university()->detach();
            $weight->university()->attach($u->id);
            $index++;

            //asset type
            $type = AssetType::create([
                'name' => 'Bàn học sinh',
                'code' => sprintf("%03d", $indexType),
                'description' => '<p>Bàn học sinh</p>',
                'created_at' => Carbon::now()
            ]);
            $type->assetAttribute()->attach($color->id,['value' => '#33333']);
            $type->assetAttribute()->attach($weight->id,['value' => '25']);
            $type->assetAttribute()->attach($material->id,['value' => 'Gỗ']);
            $type->assetAttribute()->attach($height->id,['value' => '45']);
            $type->assetAttribute()->attach($length->id,['value' => '26']);
            $type->setRelation('createBy',$administrator);
            $type->university()->detach();
            $type->university()->attach($u->id);
            $indexType++;

            $type1 = AssetType::create([
                'name' => 'Cửa sổ',
                'code' => sprintf("%03d", $indexType),
                'description' => '<p>Cửa sổ phòng học</p>',
                'created_at' => Carbon::now()
            ]);
            $type1->assetAttribute()->attach($color->id,['value' => '#22222']);
            $type1->assetAttribute()->attach($weight->id,['value' => '5']);
            $type1->assetAttribute()->attach($material->id,['value' => 'Gỗ']);
            $type1->assetAttribute()->attach($height->id,['value' => '120']);
            $type1->assetAttribute()->attach($length->id,['value' => '35']);
            $type1->setRelation('createBy',$administrator);
            $type1->university()->detach();
            $type1->university()->attach($u->id);
            $indexType++;

            //asset
            $asset = Asset::create([
                'name' => 'Bàn 01',
                'code' => sprintf("%03d", $indexAsset),
                'created_at' => Carbon::now(),
                'number' => 400,
                'status' => 2,
                'description' => '<p>Text line 1<br>line 2</p>'
            ]);
            $asset->assetType()->detach();
            $asset->assetType()->attach($type->id);
            $asset->setRelation('createBy',$administrator);
            $asset->university()->detach();
            $asset->university()->attach($u->id);
            $asset->addMedia(storage_path('tmp/uploads/ban12-'.$u->id.'.jpg'))->toMediaCollection('main_images');
            $asset->addMedia(storage_path('tmp/uploads/ban13-'.$u->id.'.jpg'))->toMediaCollection('main_images');
            $asset->addMedia(storage_path('tmp/uploads/ban14-'.$u->id.'.jpg'))->toMediaCollection('main_images');
            $indexAsset++;

            $asset1 = Asset::create([
                'name' => 'Cửa sổ 01',
                'code' => sprintf("%03d", $indexAsset),
                'created_at' => Carbon::now(),
                'number' => 35,
                'status' => 2,
                'description' => '<p>Text line 1<br>line 2</p>'
            ]);
            $asset1->assetType()->detach();
            $asset1->assetType()->attach($type1->id);
            $asset1->setRelation('createBy',$administrator);
            $asset1->university()->detach();
            $asset1->university()->attach($u->id);
            $asset1->addMedia(storage_path('tmp/uploads/cua1-'.$u->id.'.jpg'))->toMediaCollection('main_images');
            $asset1->addMedia(storage_path('tmp/uploads/cua12-'.$u->id.'.jpg'))->toMediaCollection('main_images');
            $asset1->addMedia(storage_path('tmp/uploads/cua13-'.$u->id.'.jpg'))->toMediaCollection('main_images');
            $asset1->addMedia(storage_path('tmp/uploads/cua14-'.$u->id.'.jpg'))->toMediaCollection('main_images');
            $indexAsset++;

            $asset2 = Asset::create([
                'name' =>'Cửa sổ 02',
                'code' => sprintf("%03d", $indexAsset),
                'created_at' => Carbon::now(),
                'number' => 45,
                'status' => 2,
                'description' => '<p>Text line 1<br>line 2</p>'
            ]);
            $asset2->assetType()->detach();
            $asset2->assetType()->attach($type1->id);
            $asset2->setRelation('createBy',$administrator);
            $asset2->university()->detach();
            $asset2->university()->attach($u->id);
            $asset2->addMedia(storage_path('tmp/uploads/cua2-'.$u->id.'.jpg'))->toMediaCollection('main_images');
            $asset2->addMedia(storage_path('tmp/uploads/cua22-'.$u->id.'.jpg'))->toMediaCollection('main_images');
            $asset2->addMedia(storage_path('tmp/uploads/cua23-'.$u->id.'.jpg'))->toMediaCollection('main_images');
            $asset2->addMedia(storage_path('tmp/uploads/cua24-'.$u->id.'.jpg'))->toMediaCollection('main_images');
            $indexAsset++;
        }
    }
}
