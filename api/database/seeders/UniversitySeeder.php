<?php

namespace Database\Seeders;

use App\Models\University;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UniversitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $model1 = University::create([
            'name' => 'Đại học Thái Nguyên',
            'code' => '001',
            'address' => 'Phường Tân Thịnh - Thành phố Thái Nguyên',
            'phone' => '02083852650',
            'created_at' => Carbon::now()
        ]);
        $model1->addMedia(storage_path('tmp/uploads/dhtn_logo.png'))->toMediaCollection('logo');
        $model1->addMedia(storage_path('tmp/uploads/dhtn_main_1.jpg'))->toMediaCollection('main_images');

        $model2 = University::create([
            'name' => 'Trường đại học công nghệ thông tin và truyền thông - Đại học thái nguyên',
            'code' => '002',
            'address' => 'Đường Z115, Quết Thắng, Thành phố Thái Nguyên',
            'phone' => '02083846254',
            'created_at' => Carbon::now()
        ]);
        $model2->addMedia(storage_path('tmp/uploads/dhcntt_logo.jpg'))->toMediaCollection('logo');
        $model2->addMedia(storage_path('tmp/uploads/dhcntt_main_1.jpg'))->toMediaCollection('main_images');

        $model3 = University::create([
            'name' => 'Trường Đại học Khoa học - Đại học Thái Nguyên',
            'code' => '003',
            'address' => 'Phường Tân Thịnh, TP. Thái Nguyên, Tỉnh Thái Nguyên',
            'phone' => '0989821199',
            'created_at' => Carbon::now()
        ]);
        $model3->addMedia(storage_path('tmp/uploads/dhkhxh_logo.png'))->toMediaCollection('logo');
        $model3->addMedia(storage_path('tmp/uploads/dhcn_main_1.jpg'))->toMediaCollection('main_images');

        $model4 = University::create([
            'name' => 'Trường ngoại ngữ - Đại học thái nguyên',
            'code' => '004',
            'address' => 'Xã Quyết thắng, thành phố Thái Nguyên, tỉnh Thái Nguyên, Việt Nam',
            'phone' => '02083648489',
            'created_at' => Carbon::now()
        ]);
        $model4->addMedia(storage_path('tmp/uploads/dhnn_logo.png'))->toMediaCollection('logo');
        $model4->addMedia(storage_path('tmp/uploads/dhnn_main_1.jpg'))->toMediaCollection('main_images');
    }
}
