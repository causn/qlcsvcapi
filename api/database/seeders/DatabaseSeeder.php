<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\University;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UniversitySeeder::class,
            CompanySeeder::class,
            PermissionTableSeeder::class,
            CreateAdminUserSeeder::class,
            AssetAttributeSeeder::class
        ]);
    }

    public function createAdministrator(){
        $administrator = User::query()->where('email','administrator@gmail.com')->first();
        if(!$administrator) {
            $administrator = new User();
            $administrator::$enableHistory = false;
            $administrator->name = 'administrator';
            $administrator->email = 'administrator@gmail.com';
            $administrator->password = bcrypt('12345679');
            $administrator->is_admin = 1;
            $administrator->save();
        }
        Auth::login($administrator);
        return $administrator;
    }

    public function createPermissions(){
        $permissions = [
            'user-list',
            'user-create',
            'user-edit',
            'user-delete',
            'profile',
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'permission-list',
            'permission-create',
            'permission-edit',
            'permission-delete',
            'asset-list',
            'asset-create',
            'asset-edit',
            'asset-delete',
            'asset-type-list',
            'asset-type-create',
            'asset-type-edit',
            'asset-type-delete',
            'asset-type-show',
            'asset-attribute-list',
            'asset-attribute-create',
            'asset-attribute-edit',
            'asset-attribute-delete',
            'asset-attribute-show',
            'company-list',
            'company-create',
            'company-edit',
            'company-delete',
            'liquidation-list',
            'liquidation-create',
            'liquidation-edit',
            'liquidation-delete',
            'rent-list',
            'rent-create',
            'rent-edit',
            'rent-delete',
            'university-list',
            'university-create',
            'university-edit',
            'university-delete',
        ];

        $result = [];

        foreach ($permissions as $permission) {
            $result[$permission] = Permission::create(['name' => $permission]);
        }

        return $result;
    }

    public function createRole(){
        $roles = [
            'admin' => [
                'user-list',
                'user-create',
                'user-edit',
                'user-delete',
                'profile',
                'role-list',
                'role-create',
                'role-edit',
                'role-delete',
                'permission-list',
                'permission-create',
                'permission-edit',
                'permission-delete',
                'asset-list',
                'asset-create',
                'asset-edit',
                'asset-delete',
                'asset-type-list',
                'asset-type-create',
                'asset-type-edit',
                'asset-type-delete',
                'asset-type-show',
                'asset-attribute-list',
                'asset-attribute-create',
                'asset-attribute-edit',
                'asset-attribute-delete',
                'asset-attribute-show',
                'company-list',
                'company-create',
                'company-edit',
                'company-delete',
                'liquidation-list',
                'liquidation-create',
                'liquidation-edit',
                'liquidation-delete',
                'rent-list',
                'rent-create',
                'rent-edit',
                'rent-delete'
            ],
            'director' => [
                'asset-list',
                'asset-create',
                'asset-edit',
                'asset-delete',
                'asset-type-list',
                'asset-type-create',
                'asset-type-edit',
                'asset-type-delete',
                'asset-type-show',
                'asset-attribute-list',
                'asset-attribute-create',
                'asset-attribute-edit',
                'asset-attribute-delete',
                'asset-attribute-show',
                'liquidation-list',
                'liquidation-create',
                'liquidation-edit',
                'liquidation-delete',
                'rent-list',
                'rent-create',
                'rent-edit',
                'rent-delete',
                'user-list',
                'user-edit',
                'profile'
            ],
            'manager' => [
                'asset-list',
                'asset-create',
                'asset-edit',
                'asset-delete',
                'asset-type-list',
                'asset-type-create',
                'asset-type-edit',
                'asset-type-delete',
                'asset-type-show',
                'asset-attribute-list',
                'asset-attribute-create',
                'asset-attribute-edit',
                'asset-attribute-delete',
                'asset-attribute-show',
                'liquidation-list',
                'liquidation-create',
                'liquidation-edit',
                'liquidation-delete',
                'rent-list',
                'rent-create',
                'rent-edit',
                'rent-delete',
                'profile',
                'user-list'
            ],
            'staff' => [
                'asset-list',
                'asset-create',
                'asset-edit',
                'asset-delete',
                'liquidation-list',
                'liquidation-create',
                'liquidation-edit',
                'liquidation-delete',
                'rent-list',
                'rent-create',
                'rent-edit',
                'rent-delete',
                'profile',
                'asset-type-show',
                'asset-attribute-show',
            ]
        ];
        $result = [];

        foreach ($roles as $key => $items){
            $role = Role::create(['name' => $key]);
            $permissions = Permission::query()->whereIn('name',$items)->get()->pluck('id','id');
            $role->syncPermissions($permissions);
            $result[$key] = $role;
        }

        $role = Role::create(['name' => 'administrator']);
        $permissions = Permission::query()->get()->pluck('id','id');
        $role->syncPermissions($permissions);

        return $result;
    }

    public function createUniversity(){
        $dhtn = University::query()->where('code','001')->first();
        if(!$dhtn){
            $dhtn = University::create([
                'name' => 'Đại học Thái Nguyên',
                'code' => '001',
                'address' => 'Phường Tân Thịnh - Thành phố Thái Nguyên',
                'phone' => '02083852650',
                'created_at' => Carbon::now()
            ]);
        }

        $dhcntt = University::query()->where('code','002')->first();
        if(!$dhcntt){
            $dhcntt = University::create([
                'name' => 'Trường đại học công nghệ thông tin và truyền thông - Đại học thái nguyên',
                'code' => '002',
                'address' => 'Đường Z115, Quết Thắng, Thành phố Thái Nguyên',
                'phone' => '02083846254',
                'created_at' => Carbon::now()
            ]);
        }

        $dhkh = University::query()->where('code','003')->first();
        if(!$dhkh){
            $dhkh = University::create([
                'name' => 'Trường Đại học Khoa học - Đại học Thái Nguyên',
                'code' => '003',
                'address' => 'Phường Tân Thịnh, TP. Thái Nguyên, Tỉnh Thái Nguyên',
                'phone' => '0989821199',
                'created_at' => Carbon::now()
            ]);
        }

        $dhnn = University::query()->where('code','004')->first();
        if(!$dhnn){
            $dhnn = University::create([
                'name' => 'Trường ngoại ngữ - Đại học thái nguyên',
                'code' => '004',
                'address' => 'Xã Quyết thắng, thành phố Thái Nguyên, tỉnh Thái Nguyên, Việt Nam',
                'phone' => '02083648489',
                'created_at' => Carbon::now()
            ]);
        }
        return [$dhtn,$dhcntt,$dhkh,$dhnn];
    }

    public function createCompany($universities = []){
        if(count($universities) > 0){
            foreach ($universities as $university){
                Company::create([
                    'name' => 'Công Ty TNHH Cửa Sổ Việt Châu Á Asean window Viet Asian Window Asean window Co.,Ltd',
                    'code' => '001',
                    'address' => '191 Lê Trọng Tấn, P. Sơn Kỳ, Q. Tân Phú, Tp. Hồ Chí Minh',
                    'phone' => '02862686132',
                    'created_at' => Carbon::now(),
                    'university_id' => $university->id
                ]);

                Company::create([
                    'name' => 'Việt Nam Window',
                    'code' => '002',
                    'address' => 'Toà nhà Elipse Tower, 110 Trần Phú, Hà Đông, Hà Nội.',
                    'phone' => '02462958958',
                    'created_at' => Carbon::now(),
                    'university_id' => $university->id
                ]);

                Company::create([
                    'name' => 'Nội thất Hòa Phát',
                    'code' => '003',
                    'address' => 'B-TT01-46, Khu nhà ở Ngân Hà Vạn Phúc, Phường Vạn Phúc, Quận Hà Đông, Thành phố Hà Nội, Việt Nam',
                    'phone' => '02433676688',
                    'created_at' => Carbon::now(),
                    'university_id' => $university->id
                ]);

                Company::create([
                    'name' => 'Công Ty TNHH Nội Thất Minh Đức',
                    'code' => '004',
                    'address' => 'D04 -L26, An Phú, Dương Nội, Hà Đông, Hà Nội',
                    'phone' => '0908314939',
                    'created_at' => Carbon::now(),
                    'university_id' => $university->id
                ]);
            }
        }
        return [];
    }

    public function createUsers($universities = [],$users = ['dhtn','dhnn','dhkh','dhcntt'],$roles = []){
        if(count($universities) > 0){
            foreach ($universities as $university){
                $users = ['dhtn','dhnn','dhkh','dhcntt'];
                $roleList = [];
                foreach ($users as $user){
                    $admin = User::create([
                        'name' => 'Quản trị viên',
                        'email' => 'admin.'.$user.'@gmail.com',
                        'password' => bcrypt('12345679'),
                        'level' => 0,
                        'university_id' => $university->id
                    ]);
                    $roleList['admin'][] = $admin;
                    $director = User::create([
                        'name' => 'Hiệu trưởng',
                        'email' => 'director.'.$user.'@gmail.com',
                        'password' => bcrypt('12345679'),
                        'level' => 1,
                        'university_id' => $university->id
                    ]);
                    $roleList['director'][] = $director;
                    $manager = User::create([
                        'name' => 'Trưởng khoa',
                        'email' => 'manager.'.$user.'@gmail.com',
                        'password' => bcrypt('12345679'),
                        'level' => 2,
                        'university_id' => $university->id
                    ]);
                    $roleList['manager'][] = $manager;
                    $staff = User::create([
                        'name' => 'Giảng viên',
                        'email' => 'staff.'.$user.'@gmail.com',
                        'password' => bcrypt('12345679'),
                        'level' => 3,
                        'university_id' => $university->id
                    ]);
                    $roleList['staff'][] = $staff;
                }

                foreach ($roles as $key => $role){
                    if(isset($roleList[$key])){
                        foreach ($roleList[$key] as $r){
                            $r->assignRole([$role->id]);
                        }
                    }
                }

            }
        }
        return [];
    }
}
