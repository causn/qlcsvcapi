import { createApp } from 'vue'
import i18n from './i18n'
import { createVuestic } from 'vuestic-ui'
import { createGtm } from '@gtm-support/vue-gtm'
import stores from './stores'
import router from './router'
import vuesticGlobalConfig from './services/vuestic-ui/global-config'
import App from './App.vue'
import {useGlobalStore} from "./stores/global-store";
import axios from 'axios'


const app = createApp(App)
app.use(stores)
app.use(router)
app.use(i18n)
app.use(createVuestic({ config: vuesticGlobalConfig }))

const GlobalStore= useGlobalStore();

if (import.meta.env.VITE_APP_GTM_ENABLED) {
  app.use(
    createGtm({
      id: import.meta.env.VITE_APP_GTM_KEY,
      debug: false,
      vueRouter: router,
    }),
  )
}

if(import.meta.env.VITE_API_BASE_URL){
  axios.defaults.baseURL = import.meta.env.VITE_API_BASE_URL;
}


router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // @ts-ignore
    let auth = JSON.parse(localStorage.getItem('auth'));
    if (auth?.token === null || auth?.token === undefined || auth?.token === '') {
        next({
          path: '/login',
          query: { redirect: to.fullPath }
        })
    } else {
      const publicPages = ['/auth/login', '/auth/signup'];
      const authRequired = publicPages.includes(to.path);
      if(authRequired) {
        next({
          path: '/',
          query: { redirect: to.fullPath }
        })
      }else{
        next()
      }
    }
  } else {
    const publicPages = ['/auth/login', '/auth/signup'];
    const authRequired = publicPages.includes(to.path);
    if(authRequired) {
      next({
        path: '/',
        query: { redirect: to.fullPath }
      })
    }else{
      next()
    }
  }
})

app.mount('#app')
