import {_} from 'lodash';
import apiRoute from '../../config/api.json';
export default class Api {
  baseUrl = {};
  routeConfig = {};
  constructor() {
    this.baseUrl = import.meta.env.API_BASE_URL;
    this.routeConfig = this.parseConfig();
  }

  parseConfig(){
    if(apiRoute && apiRoute.item) {
      apiRoute.item.forEach(function callback(value, index) {
        if (_.snakeCase(value.name) === 'auth') {
          apiRoute.auth = value;
        }
        if (_.snakeCase(value.name) === 'back_end') {
          apiRoute.back_end = value;
        }
        if (_.snakeCase(value.name) === 'front_end') {
          apiRoute.front_end = value;
        }
      });
    }
    return apiRoute;
  }
  getConfigByName(config, nameRoute, nameFolder){
    let result = {};

    if(_.snakeCase(config.name) === 'auth' || _.snakeCase(config.name) === 'front_end') {
      config.item.forEach(function callback(value, index) {
        if (_.snakeCase(value.name) === nameRoute) {
          result = value;
        }
      });
    }

    if(_.snakeCase(config.name) === 'back_end'){
      config.item.forEach(function callback(value, index) {
        if (_.snakeCase(value.name) === nameFolder) {
            value.item.forEach(function callback(value1, index1) {
              if (_.snakeCase(value1.name) === nameRoute) {
                result = value1;
              }
            });
        }
      });
    }

    return result;
  }
}
